// standard light controls
var video;
var hdmode = false;
var playing = false;

$(function() {
	$('#ctlPlayPause').on('click', function() {
		if (playing) {
			video.pause();
			playing = false;
			$(this).removeClass('fa-pause').addClass('fa-play');
		}
		else {
			video.play();
			playing = true;
			$(this).removeClass('fa-play').addClass('fa-pause');
		}
	});
	$('#ctlVolume').on('click', function() {
		if (Math.abs(video.volume - 1.0) < 0.1) {
			video.volume = 0.0;
			$(this).removeClass('fa-volume-up').addClass('fa-volume-off');
		}
		else {
			video.volume = 1.0;
			$(this).removeClass('fa-volume-off').addClass('fa-volume-up');
		}
	});
	$('#ctlQuality').on('click', function() {
		var time = video.currentTime;
		if (hdmode) {
			hdmode = false;
			video.src = sdroot;
			$(this).css('color', 'inherit').text(' SD');
		}
		else {
			hdmode = true;
			video.src = hdroot;
			$(this).css('color', 'black').text(' HD');
		}
		video.currentTime = time;
	});
	$('#ctlTime').on('change', function() {
		video.currentTime = $(this).val();
	});
	setInterval('timeUpdate()', 1000);
});
function timeUpdate() {
	$('#ctlTime').prop('max', Math.floor(video.duration)).prop('value', Math.floor(video.currentTime));
	
	var time = Math.floor(video.currentTime);
	var minutes = Math.floor(time / 60);
	var seconds = time - (minutes * 60);
	var ctime = str_pad_left(minutes,'0',2) + ':' + str_pad_left(seconds,'0',2);
	time = Math.floor(video.duration);
	minutes = Math.floor(time / 60);
	seconds = time - (minutes * 60);
	var dtime = str_pad_left(minutes,'0',2) + ':' + str_pad_left(seconds,'0',2);
	$('#ctlTimeText').text(
		ctime
		+ ' / ' + 
		dtime
	);
}
function str_pad_left(string,pad,length){ return (new Array(length+1).join(pad)+string).slice(-length); }

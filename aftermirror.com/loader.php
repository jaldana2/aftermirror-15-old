<?php
$app = "Home";
$launchMode = "app";
$__log["info"][] = sprintf("Loader defaults: app=%s, launchMode=%s", $app, $launchMode);

$__log["info"][] = "Checking app/core flags...";
if (isset($_GET["app"])) {
	if (file_exists(BIN_DIRECTORY . $_GET["app"] . ".php")) {
		$app = $_GET["app"];
		$__log["info"][] = "App flag changed to {$app}";
	}
	else {
		$__log["warn"][] = "App flag '{$_GET['app']}' found, but file does not exist.";
	}
}
elseif (isset($_GET["core"])) {
	if (file_exists(BIN_DIRECTORY . $_GET["core"] . ".php")) {
		$app = $_GET["core"];
		$launchMode = "core";
		$__log["info"][] = "Core flag enabled, changed to {$app}";
	}
	else {
		$__log["warn"][] = "Core flag '{$_GET['core']}' found, but file does not exist.";
	}
}

switch($app) {
	case "Sendify":
		if (!$authID) { header("Location: /app.Login"); }
	break;
}

if ($launchMode == "app") {
	$__log["info"][] = "Launching app {$app}";
	include(BIN_DIRECTORY . "HEADER.php");
	include(BIN_DIRECTORY . $app . ".php");
	$__log["info"][] = "@@ This is the last message visible in FOOTER @@";
	include(BIN_DIRECTORY . "FOOTER.php");
}
else {
	$__log["info"][] = "Launching core {$app}";
	include(BIN_DIRECTORY . $app . ".php");
	if (UX_DEBUG) {
		if (isSomething($__log["error"])) {
			echo "
				<div class='debug dbg_error'>
					<b>ERROR</b>
					<br/>
					<ul>
			";
			foreach ($__log["error"] as $message) {
				echo "<li>{$message}</li>";
			}
			echo "
					</ul>
				</div>
			";
		}
		if (isSomething($__log["warn"])) {
			echo "
				<div class='debug dbg_warn'>
					<b>WARN</b>
					<br/>
					<ul>
			";
			foreach ($__log["warn"] as $message) {
				echo "<li>{$message}</li>";
			}
			echo "
					</ul>
				</div>
			";
		}
		if (isSomething($__log["info"])) {
			echo "
				<div class='debug dbg_info'>
					<b>INFO</b>
					<br/>
					<ul>
			";
			foreach ($__log["info"] as $message) {
				echo "<li>{$message}</li>";
			}
			echo "
					</ul>
				</div>
			";
		}
	}
}

$__log["info"][] = "Loader finished executing";
?>

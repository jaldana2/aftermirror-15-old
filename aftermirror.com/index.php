<?php
session_start();

if (file_exists("../db/local.key")) {
	define("UX_LOCAL", true);
}
else {
	define("UX_LOCAL", false);
}

define("UX_DEBUG", false);
define("ENGINE_PLUGIN_DIR", "../plugins/");
define("BIN_DIRECTORY", "bin/");
if (UX_LOCAL) {
	define("SRV_HOST", "entity.aftermirror.com");
	define("SRV_PREFIX", "entity.");
}
else {
	define("SRV_HOST", "aftermirror.com");
	define("SRV_PREFIX", "");
}
define("SYS_REGISTER_INVITE_ONLY", true);
define("SYS_REGISTER_INVITE_CODE", trim(file_get_contents("../db/invite.key")));
define("UX_SALT", trim(file_get_contents("../db/secret.key")));

$__log = array();
$__log["info"][] = "Start @ " . time();
$__log["info"][] = "Debug Flag: " . (UX_DEBUG ? "Y" : "N");
$__log["info"][] = "Plugin Directory: " . ENGINE_PLUGIN_DIR;
$__log["info"][] = "Bin Directory: " . BIN_DIRECTORY;

$__log["info"][] = "Loading [engine]...";
include("../engine.php");
$__log["info"][] = "Done loading [engine]";

$authID = false;

$__log["info"][] = "Checking for \$_SESSION user data...";
if (isset($_SESSION["authUser"]) && isset($_SESSION["authKey"]) && isset($_SESSION["authCheck"])) {
	$__log["info"][] = "User data found!";
	if (sha1($_SESSION["authUser"] . $_SESSION["authKey"] . UX_SALT) != $_SESSION["authCheck"]) {
		// Integrity invalid
		session_destroy();
		$__log["err"][] = "User data integrity invalid";
		header("Location: /app.Login?username={$_SESSION['authUser']}&error=" . AM_STATUS_FAILED_LOGIN);
		die();
	}
	$__log["info"][] = "User data integrity valid";	
	$authID = new authID($_SESSION["authUser"]);
	$authID->setSessionKey($_SESSION["authKey"]);
	if (!$authID->verifyLogin()) {
		// Failed to login
		session_destroy();
		header("Location: /app.Login?username={$_SESSION['authUser']}&error=" . AM_STATUS_FAILED_LOGIN);
		die();
	}
	else {
		$__log["info"][] = sprintf("[authID] successfully logged in as %s", $authID->getUsername());
	}
}
elseif (isset($_COOKIE["authUser"]) && isset($_COOKIE["authKey"]) && isset($_COOKIE["authCheck"])) {
	$__log["info"][] = "(cookie) User data found!";
	if (sha1($_COOKIE["authUser"] . $_COOKIE["authKey"] . UX_SALT) != $_COOKIE["authCheck"]) {
		// Integrity invalid
		setcookie("authUser", "", time() - 3600);
		setcookie("authKey", "", time() - 3600);
		setcookie("authCheck", "", time() - 3600);
		$__log["err"][] = "(cookie) User data integrity invalid";
		header("Location: /app.Login?username={$_SESSION['authUser']}&error=" . AM_STATUS_FAILED_LOGIN);
		die();
	}
	$__log["info"][] = "(cookie) User data integrity valid";	
	$authID = new authID($_COOKIE["authUser"]);
	$authID->setSessionKey($_COOKIE["authKey"]);
	if (!$authID->verifyLogin()) {
		// Failed to login
		setcookie("authUser", "", time() - 3600);
		setcookie("authKey", "", time() - 3600);
		setcookie("authCheck", "", time() - 3600);
		header("Location: /app.Login?username={$_SESSION['authUser']}&error=" . AM_STATUS_FAILED_LOGIN);
		die();
	}
	else {
		$__log["info"][] = sprintf("[authID] (cookie) successfully logged in as %s", $authID->getUsername());
		$_SESSION["authUser"] = $_COOKIE["authUser"];
		$_SESSION["authKey"] = $authID->getSessionKey();
		$_SESSION["authCheck"] = sha1($_COOKIE["authUser"] . $_COOKIE["authKey"] . UX_SALT);
	}
}
else {
	$__log["info"][] = "No user data found";
}

$__log["info"][] = "Starting loader...";
include("loader.php");
$__log["info"][] = "Finished @ " . time();
?>

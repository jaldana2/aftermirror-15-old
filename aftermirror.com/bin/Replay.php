<?php
// Test

$songs = array(
	array(
		"title" => "Love Mode",
		"artist" => "Audition",
		"bpm" => 190,
		"start" => 2.25
	)
);

$song = $songs[0];

echo "
<h1>{$song['artist']} - {$song['title']}</h1>

<audio controls id='player'>
	<source src='/data/replay/test.m4a' type='audio/m4a' />
	<source src='/data/replay/test.mp3' type='audio/mp3' />
</audio>

<style type='text/css'>
	div.beatmap {
		border: double 3px #831919;
		border-radius: 50px;
		height: 50px;
		width: 80%;
		background-color: rgba(131, 25, 25, 0.4);
		margin: auto;
		text-align: center;
		padding: 6px 0px;
	}
	span.beat {
		width: 50px;
		height: 50px;
		border-radius: 50px;
		background-color: blue;
		background: linear-gradient(to bottom, #2224aa 0%,#89dfff 100%);
		display: inline-block;
		font-size: 45px;
		text-align: center;
		color: #EFEFEF;
		text-shadow: 1px 1px gray;
	}
	span.beat.pass {
		background-color: green;
		background: linear-gradient(to bottom, #bfd255 0%,#8eb92a 50%);
	}
	
	div#beatclock_container {
		width: 80%;
		height: 25px;
		position: relative;
		margin: auto;
		margin-bottom: 15px;
	}
	div#beatclock {
		position: absolute;
		right: 0px;
		top: 0px;
		height: 25px;
		width: 300px;
		background-color: rgba(0, 0, 0, 0.75);
		border: double 3px #AAA;
		border-radius: 25px;
	}
	div#beatclock_strobe {
		position: absolute;
		right: 50px;
		top: 3px;
		width: 100px;
		height: 19px;
		background-color: blue;
		background: linear-gradient(to right, #00a8ea 0%,#ffffff 50%,#00aded 100%);
		opacity: 0;
	}
	div#beatclock_pos {
		position: absolute;
		left: 0px;
		top: 0px;
		width: 15px;
		height: 15px;
		border-radius: 25px;
		background-color: #B24949;
		border: double 5px #DDD;
	}
</style>

<div id='beatclock_container'>
	<div id='beatclock'>
		<div id='beatclock_strobe'></div>
		<div id='beatclock_pos'></div>
	</div>
</div>
<div class='beatmap'>
	<span class='beat pass fa fa-arrow-left'></span>
	<span class='beat pass fa fa-arrow-right'></span>
	<span class='beat fa fa-arrow-left'></span>
	<span class='beat fa fa-arrow-right'></span>
	<span class='beat fa fa-arrow-left'></span>
	<span class='beat fa fa-arrow-right'></span>
</div>

<div id='beatlist'>

</div>

<script>
	var delta_per_second = 5;
	var beatclock_size = 275; // width - size
	var target_fps = 60;
	var sopa_dim = false;
	var sopa_delta_per_second = 0.01;
	var play_level = 1;
	
	$(function() {
		$('#player')[0].play();
		$('#player').on('play', function() {
			setTimeout(\"beatgen({$song['bpm']})\", 1000 * {$song['start']});
		});
		$('body').keydown(function(e) {
			if (e.which == 37) { // left
				beatpress('left');
			}
			else if (e.which == 38) { // up
				beatpress('up');
			}
			else if (e.which == 40) { // down
				beatpress('down');
			}
			else if (e.which == 39) { // right
				beatpress('right');
			}
			else if (e.which == 32 || e.which == 17) { // space or left ctrl
				beatpress('space');
			}
		});
	});
	
	function beatgen(bpm) {
		time_per_tick = target_fps / bpm;
		delta_per_second = beatclock_size / time_per_tick / target_fps / 3.75;
		sopa_delta_per_second = 60 / bpm / 3;
		setInterval(\"beattick()\", 1000 / target_fps);
	}
	function beattick() {
		var lpos = $('#beatclock_pos').position().left;
		if (lpos >= beatclock_size) {
			// rewind
			$('#beatclock_pos').css('left', '0px');
		}
		else {
			$('#beatclock_pos').css('left', (lpos + delta_per_second) + 'px');
		}
		
		var sopa = $('#beatclock_strobe').css('opacity');
		if (sopa >= 1.0) {
			sopa_dim = true;
		}
		if (sopa <= 0.05) {
			sopa_dim = false;
		}
		if (sopa_dim == true) {
			$('#beatclock_strobe').css('opacity', sopa - sopa_delta_per_second);
		}
		else {
			$('#beatclock_strobe').css('opacity', sopa - (-sopa_delta_per_second));
		}
	}
	function beatpress(dir) {
	
	}
</script>
";
?>

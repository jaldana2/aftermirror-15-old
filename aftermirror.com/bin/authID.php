<?php
// Simple authID API
session_write_close();

// Pre-allocate
$username = "";
$password = "";
$sessionKey = "";

// Clean all values
if (isset($_GET["username"])) {
	$username = sanitize(cleanANString(strtolower($_GET["username"])));
}
if (isset($_GET["password"])) {
	$password = $_GET["password"];
}
if (isset($_GET["sessionKey"])) {
	$sessionKey = sanitize($_GET["sessionKey"]);
}

if (isset($_GET["do"])) switch($_GET["do"]) {
	case "AUTH":
		$exAuthID = new authID($username);
		if (isSomething($password)) {
			$exAuthID->setPassword($password);
			if ($exAuthID->verifyLogin()) {
				echo $exAuthID->getSessionKey();
			}
			else {
				echo "FAIL";
			}
		}
		else {
			echo "FAIL";
		}
	break;
	case "VERIFY":
		$exAuthID = new authID($username);
		if (isSomething($sessionKey)) {
			$exAuthID->setSessionKey($sessionKey);
			if ($exAuthID->verifyLogin()) {
				echo "PASS";
			}
			else {
				echo "FAIL";
			}
		}
		else {
			echo "FAIL";
		}
	break;
	default:
		echo "0";
	break;
}
die(); // prevent any other output.
?>

<?php
echo "
<h1>sendify.me</h1>
<div class='nav'>
	<a href='?new=url'>url</a>
	<a href='?new=image'>image</a>
	<a href='?new=audio'>audio</a>
	<a href='?new=video'>video</a>
	<a href='?new=file'>file</a>
</div>
";
if (isset($_GET["new"])) {
	echo "
		<form action='?sendify' method='post' class='login' enctype='multipart/form-data'>
		<label for='sendkey'>https://" . SRV_PREFIX . "sendify.me/<b id='sendkey_txt'>...</b></label>
		<input type='text' name='sendkey' id='sendkey' />
	";
	switch($_GET["new"]) {
		case "url":
			echo "
				<input type='hidden' name='type' value='url' />
				<br/>
				<label for='value'>url (e.g.: http://google.com/)</label>
				<input type='text' name='value' id='value' />
			";
		break;
		case "image":
			echo "
				<input type='hidden' name='type' value='image' />
				<br/>
				<label for='value'>image file</label>
				<input type='file' name='value' id='file' accept='image/*' />
			";
		break;
		case "audio":
			echo "
				<input type='hidden' name='type' value='audio' />
				<br/>
				<label for='value'>audio file</label>
				<input type='file' name='value' id='file' accept='audio/*' />
			";
		break;
		case "video":
			echo "
				<input type='hidden' name='type' value='video' />
				<br/>
				<label for='value'>video file</label>
				<input type='file' name='value' id='file' accept='video/*' />
			";
		break;
		case "file":
			echo "
				<input type='hidden' name='type' value='file' />
				<br/>
				<label for='value'>file (cannot be more than 100MB)</label>
				<input type='file' name='value' id='file' />
			";
		break;
		default:
		break;
	}
	echo "
			<br/>
			<input type='submit' value='sendify!' class='button' />
		</form>
		<script>
			$(function() {
				$('#sendkey').on('keyup', function() {
					$('#sendkey_txt').text($(this).val());
				});
			});
		</script>
	";
}
elseif (isset($_GET["sendify"]) && isset($_POST["type"])) {
	$sendify = new Sendify();
	$key = $_POST["sendkey"];
	if (!isset($key) || !isSomething($key)) {
		$key = $sendify->randKey();
	}
	
	if (!$sendify->keyExists($key)) {
		$sendify->addKey($key, $_POST["type"], $authID->getUsername(), getIPAddr());
		switch ($_POST["type"]) {
			case "url":
				file_put_contents("../webpack.ninja/data/sendify/{$key}.url", $_POST["value"]);
			break;
			case "image":
			case "audio":
			case "video":
			case "file":
				$target = "../webpack.ninja/data/sendify/{$key}.dat";
				if (!move_uploaded_file($_FILES["value"]["tmp_name"], $target)) {
					echo "<b>Error: Failed to transfer file.</b><br/>";
					print_a($_FILES);
				}
				$sendify->setOriginal($key, $_FILES["value"]["name"]);
				$sendify->setMimetype($key, $_FILES["value"]["type"]);
			break;
		}
		echo "<h3><a href='//" . SRV_PREFIX . "sendify.me/{$key}'>" . SRV_PREFIX . "sendify.me/{$key}</a></h3>";
	}
	else {
		echo "<p class='error'>Failed!</p>";
	}
}
else {
	echo "<p>Select what you want to sendify above.</p>";
}
?>

<?php
$workdir = "/var/www/webpack.ninja/data/youtubed/";

if (isset($_GET["download"]) && isset($_GET["ext"])) {
	$file = $workdir . $_GET["download"] . $_GET["ext"];
	if (file_exists($file)) {
		header("Content-length: " . filesize($file));
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="' . $_GET["download"] . $_GET["ext"]. '"');
		readfile($file);
	}
}
?>
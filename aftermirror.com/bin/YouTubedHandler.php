<?php
session_write_close();

$video = $_GET["video"];
$convert = $_GET["convert"];
$workdir = "/var/www/webpack.ninja/data/youtubed/";

if (file_exists($workdir . ".queue")) {
	echo "<h3>Processing...</h3>";
	echo "
	<script>
		setTimeout('refreshProc();', 2000);
	</script>
	";
}
elseif (isset($_GET["init"])) {
	set_time_limit(180);
	ignore_user_abort();
	file_put_contents($workdir . "{$video}-{$convert}.queue", time());

	$q = "";
	switch ($convert) {
		case "mp3_vbr":
			if (file_exists("{$workdir}{$video}-vbr.mp3")) {
				$q = false;
			}
			else {
				$q = "youtube-dl --max-filesize 100m -x --audio-format mp3 --audio-quality 0 -o \"{$workdir}%(id)s-vbr.$(ext)s\" {$video}";
			}
		break;
		case "mp3_320":
			if (file_exists("{$workdir}{$video}-320.mp3")) {
				$q = false;
			}
			else {
				$q = "youtube-dl --max-filesize 100m -x --audio-format mp3 --audio-quality 320K -o \"{$workdir}%(id)s-320.$(ext)s\" {$video}";
			}
		break;
		case "mp3_192":
			if (file_exists("{$workdir}{$video}-192.mp3")) {
				$q = false;
			}
			else {
				$q = "youtube-dl --max-filesize 100m -x --audio-format mp3 --audio-quality 192K -o \"{$workdir}%(id)s-192.$(ext)s\" {$video}";
			}
		break;
	}
	if (!$q) {
		// exists already
	}
	else {
		exec($q);
	}
	unlink($workdir . "{$video}-{$convert}.queue");
	// might fire later.
	echo "<h3>Loading...</h3>";
	echo "
	<script>
		setTimeout('refreshProc();', 1000);
	</script>
	";
}
else {
	$ext = "unknown";
	switch ($convert) {
		case "mp3_vbr":
			$ext = "-vbr.mp3";
		break;
		case "mp3_320":
			$ext = "-320.mp3";
		break;
		case "mp3_192":
			$ext = "-192.mp3";
		break;
	}
	if (file_exists($workdir . $video . $ext)) {
		echo "<h3>Complete!</h3>";
		echo "<a href='core.YouTubedPostHandler?download={$video}&ext={$ext}'>Download</a>";
	}
	else {
		echo "<h3>Loading...</h3>";
		echo "
		<script>
			setTimeout('refreshProc();', 1000);
		</script>
		";
	}
}
?>
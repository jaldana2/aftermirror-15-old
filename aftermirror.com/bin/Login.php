<?php
$username = "";
if (isset($_GET["username"])) $username = $_GET["username"];

if (isset($_GET["error"])) {
	echo "<p class='error'><b><span class='fa fa-exclamation-triangle'></span> Error:</b> ";
	switch($_GET["error"]) {
		case AM_STATUS_FAILED_LOGIN:
			echo "Login failure";
		break;
		case AM_STATUS_NO_MATCH:
			echo "Invalid login";
		break;
		default:
			echo "Unknown error";
		break;
	}
	echo "</p><br/>";
}

if (isset($_GET["redirect"])) {
	if (!$authID) {
		echo "
		<h1>login</h1>
		<p>You need an after|mirror account to use this website.</p>
		<br/>
		<p>[ return to <a href='//{$_GET['redirect']}'>{$_GET['redirect']}</a> ]</p>
		<br/>
		<form action='core.LoginHandler?do=login' method='post' class='login'>
			<input type='hidden' name='redirect' value='{$_GET['redirect']}' />
			<label for='username'>username</label>
			<input type='text' name='username' id='password' value='{$username}' />
			<label for='password'>password</label>
			<input type='password' name='password' id='password' />
			<br/>
			<input type='checkbox' name='remember' id='remember' /> 
			<label for='remember'>keep me logged in</label>
			<input type='submit' value='login' class='button' />
			<br/>
			<p>Don't have an account? <a href='app.Register'>Create a new account.</a>
			<br/>
			Forgot your password? <a href='app.PasswordRecovery'>Recover your account.</a></p>
		</form>
		";
	}
	else {
		printf("<h1>Please wait...</h1><script>setTimeout(\"document.location = '//{$_GET['redirect']}/core.rAuth?username=%s&key=%s&return=%s';\", 1500);</script>", $authID->getUsername(), $authID->getSessionKey(), $_GET["return"]);
	}
}
else {
	if (!$authID) {
		echo "
		<h1>welcome back!</h1>
		<p>You need an account to use this website. If you need an account, click the below to create a new account.</p>
		<br/>
		<form action='core.LoginHandler?do=login' method='post' class='login'>
			<label for='username'>username</label>
			<input type='text' name='username' id='password' value='{$username}' />
			<label for='password'>password</label>
			<input type='password' name='password' id='password' />
			<br/>
			<input type='checkbox' name='remember' id='remember' /> 
			<label for='remember'>keep me logged in</label>
			<input type='submit' value='login' class='button' />
			<br/>
			<p>Don't have an account? <a href='app.Register'>Create a new account.</a>
			<br/>
			Forgot your password? <a href='app.PasswordRecovery'>Recover your account.</a></p>
		</form>
		";
	}
	else {
		echo "<script>document.location = 'app.Home';</script>";
	}
}
?>

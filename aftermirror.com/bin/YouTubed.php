<?php
echo "
<h1>webpack.ninja</h1>
<p><b>YouTubed</b> // yet another YouTube downloader</p>
<br/>
";
$page = "default";
if (isset($_GET["page"])) $page = $_GET["page"];
switch ($page) {
	case "convert":
		// no live stats for now.
		if (strlen($_POST["url"]) == 11) {
			switch ($_POST["convert"]) {
				case "mp3_vbr":
				case "mp3_320":
				case "mp3_192":
					echo "<div id='proc_box'><h3>Please wait...</h3></div>";
					echo "
					<br/>
					<script>
						$(function() {
							$('#proc_box').load('core.YouTubedHandler?video={$_POST['url']}&convert={$_POST['convert']}&init');
							refreshProc();
						});
						function refreshProc() {
							$('#proc_box').load('core.YouTubedHandler?video={$_POST['url']}&convert={$_POST['convert']}');
						}
					</script>
					";
				break;
				default:
					echo "<h3>Invalid Process Type</h3>";
					echo "<p>If this problem persists, something is wrong with the downloader.</p>";
				break;
			}
		}
		else {
			echo "<h3>Invalid YouTube ID</h3>";
			echo "<p>Please make sure you input only the 11 digit YouTube ID found at the end of the YouTube URL.</p>";
		}
	break;
}
echo "
<form action='?page=convert' method='post' class='login'>
	<label for='url'>YouTube ID</label>
	<input type='text' name='url' id='url' placeholder='https://youtube.com/watch?v=___________' />
	<br/>
	<label><b>Process Type</b></label>
	<br/>
	<input type='radio' name='convert' id='mp3_320' value='mp3_320' /> <label for='mp3_320'>320 Kbps *.mp3</label>
	<br/>
	<input type='radio' name='convert' id='mp3_192' value='mp3_192' /> <label for='mp3_192'>192 Kbps *.mp3</label>
	<br/>
	<input type='radio' name='convert' id='mp3_vbr' value='mp3_vbr' checked /> <label for='mp3_vbr'>VBR *.mp3</label>
	<br/>
	<input type='submit' value='Process' class='button' />
</form>
";
?>
<?php
if ($authID) { echo "<script>window.location = 'app.Home';</script>"; }

echo "<h1>register</h1>";

if (SYS_REGISTER_INVITE_ONLY && (!isset($_GET["invite"]) || ($_GET["invite"] !== SYS_REGISTER_INVITE_CODE))) {  
	echo "
	<p>Currently, you can only sign up for a new account if you have an invite code.
	<br/>
	<br/>
	If you have an invite code, please enter it below.</p>
	<br/>
	<form action='/app.Register' method='get' class='login'>
		<label for='invite'>invite code</label>
		<input type='text' name='invite' id='invite' />
		<br/>
		<input type='submit' value='verify invite code' class='button' />
	</form>
	";
}
else {
	if (!isset($_GET["invite"])) $invite = "";
	else $invite = $_GET["invite"];
	$username = "";
	$password = "";
	$authPin = 0;
	$email = "";
	
	if (isset($_POST["username"])) {
		$username = strtolower(cleanANString($_POST["username"]));
		$password = $_POST["password"];
		$validPassword = ($_POST["password"] == $_POST["password2"]);
		$authPin = (int) $_POST["authPin"];
		$validPin = ($authPin >= 0 && $authPin <= 9999);
		$email = $_POST["email"];
		$validEmail = isValidEmail($email);
		
		$valid = true;
		$errors = array();
		if (strlen($username) < 3 || strlen($username) > 20) {
			$errors[] = "Username too short/long.";
			$valid = false;
		}
		switch ($username) {
			case "admin":
			case "administrator":
			case "bot":
			case "guest":
			case "login":
			case "logout":
			case "master":
			case "me":
			case "mod":
			case "moderator":
			case "postmaster":
			case "vip":
			case "webmaster":
				$errors[] = "Reserved username.";
				$valid = false;
			break;
		}
		if (!$validPassword) {
			$errors[] = "Passwords do not match.";
			$valid = false;
		}
		if (strlen($password) < 1) {
			$errors[] = "Password too short.";
			$valid = false;
		}
		if (isSomething($_POST["authPin"]) && !$validPin) {
			$errors[] = "PIN is set but is invalid.";
			$valid = false;
		}
		if (isSomething($_POST["email"]) && !$validEmail) {
			$errors[] = "Email is set but is invalid.";
			$valid = false;
		}
		$authID = new authID("master");
		$authID->initDB();
		if ($authID->userExists($username)) {
			$errors[] = "Username exists.";
			$valid = false;
		}
		
		if (!$valid) {
			echo "<p class='error'>We could not make your account:";
			foreach ($errors as $error) {
				echo "<br/><b>{$error}</b>";
			}
			echo "</p>";
		}
		else {
			echo "<p class='error'>Your account has been created! You will be redirected shortly.</p>";
			$authID = new authID($username);
			$authID->addUser($username, $password, $authPin, $email);
			
			echo "<script>setTimeout(\"window.location = 'app.Login?username={$username}';\", 4000);</script>";
		}
	}
	
	echo "
	<p>Everything with an asterisk(*) is required.</p>
	<br/>
	<form action='/app.Register?invite={$invite}' method='post' class='login'>
		<label for='username'>username* (3&mdash;20 characters)</label>
		<input type='text' name='username' id='username' value='{$username}' />
		<br/>
		<label for='password'>password* (must be at least one character)</label>
		<input type='password' name='password' id='password' />
		<br/>
		<label for='password2'>password* again (this is your last chance)</label>
		<input type='password' name='password2' id='password2' />
		<br/>
		<label for='authPin'>PIN (used for recovery; numbers only, 1&mdash;4 digits)</label>
		<input type='number' name='authPin' id='authPin' min='0' max='9999' maxlength='4' size='4' value='{$authPin}' />
		<br/>
		<label for='email'>email (used for recovery)</label>
		<input type='text' name='email' id='email' value='{$email}' />
		<br/>
		<input type='submit' value='register' class='button' />
	</form>
	";
}
?>

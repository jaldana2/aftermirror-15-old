<!DOCTYPE html>
<html>
<head>
	<title>after|mirror</title>
	<link href='//<?php echo SRV_HOST; ?>/static/style.css' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
	<meta name="viewport" content="width=360, initial-scale=1, maximum-scale=1, minimum-scale=1">
	<link href='https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css' rel='stylesheet' type='text/css'>
	<script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
	<script>
		
	</script>
</head>
<body>
	<div id="content">
		<div class="background" style="height: 100px;"></div>
		<h1 class="header"><a href='/'><em>after</em>|mirror</a></h1>
		<div id="navigation">
			<a href="/" class="current"><span class='fa fa-home'></span></a>
			<a href="//<?php echo SRV_PREFIX; ?>urusai.ninja/">anime + ost</a>
			<a href="/app.Webpack">webpack</a>
			<a href="/app.Sendify">url shortener</a>
<?php
	if (!$authID) {
		echo "
			<a href=\"/app.Login\">login</a>
		";
	}
	else {
		printf("
			<a href=\"/app.Profile\">%s</a>
			<a href=\"/app.Settings\"><span class='fa fa-gear'></span></a>
			<a href=\"/core.LoginHandler?do=logout\" style='color: #DD5656;'><span class='fa fa-power-off'></span></a>
		", $authID->getUsername());
	}
?>
		</div>

		<div id="footer">
			<a href="//<?php echo SRV_HOST; ?>">after|mirror</a>
		</div>
	</div>
<?php
if (UX_DEBUG) {
	if (isSomething($__log["error"])) {
		echo "
			<div class='debug dbg_error'>
				<b>ERROR</b>
				<br/>
				<ul>
		";
		foreach ($__log["error"] as $message) {
			echo "<li>{$message}</li>";
		}
		echo "
				</ul>
			</div>
		";
	}
	if (isSomething($__log["warn"])) {
		echo "
			<div class='debug dbg_warn'>
				<b>WARN</b>
				<br/>
				<ul>
		";
		foreach ($__log["warn"] as $message) {
			echo "<li>{$message}</li>";
		}
		echo "
				</ul>
			</div>
		";
	}
	if (isSomething($__log["info"])) {
		echo "
			<div class='debug dbg_info'>
				<b>INFO</b>
				<br/>
				<ul>
		";
		foreach ($__log["info"] as $message) {
			echo "<li>{$message}</li>";
		}
		echo "
				</ul>
			</div>
		";
	}
}
?>
</body>
</html>

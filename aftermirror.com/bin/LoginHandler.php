<?php
if (isset($_GET["do"])) switch($_GET["do"]) {
	case "logout":
		session_start();
		session_destroy();
		setcookie("authUser", "", time() - 3600);
		setcookie("authKey", "", time() - 3600);
		setcookie("authCheck", "", time() - 3600);
		if (isset($_GET["redirect"])) {
			header("Location: //{$_GET['redirect']}/?goodbye");
		}
		else {
			header("Location: app.Home?goodbye");
		}
	break;
	case "login":
		if (isset($_POST["username"]) && isset($_POST["password"])) {
			// sanitize username
			$username = strtolower($_POST["username"]);
			$username = cleanANString($_POST["username"]);
			$loginID = new authID($username);
			$loginID->setPassword($_POST["password"]);
			if ($loginID->verifyLogin()) {
				$_SESSION["authUser"] = $username;
				$_SESSION["authKey"] = $loginID->getSessionKey();
				$_SESSION["authCheck"] = sha1($_SESSION["authUser"] . $_SESSION["authKey"] . UX_SALT);
				// remember if need be
				if (isset($_POST["remember"]) && $_POST["remember"] == "on") {
					$exp = time() + 3600 * 24 * 365;
					setcookie("authUser", $username, $exp, "/");
					setcookie("authKey", $loginID->getSessionKey(), $exp, "/");
					setcookie("authCheck", sha1($_SESSION["authUser"] . $_SESSION["authKey"] . UX_SALT), $exp, "/");
				}
				
				if (isset($_POST["redirect"])) {
					header(sprintf("Location: //{$_POST['redirect']}/core.rAuth?username=%s&key=%s", $loginID->getUsername(), $loginID->getSessionKey()));
				}
				else {
					header("Location: app.Home?hello");
				}
			}
			else {
				if (isset($_POST["redirect"])) {
					header("Location: app.Login?redirect={$_POST['redirect']}&username={$username}&error=" . AM_STATUS_NO_MATCH);
				}
				else {
					header("Location: app.Login?username={$username}&error=" . AM_STATUS_NO_MATCH);
				}
			}
		}
	break;
	default:
		header("Location: app.Home");
	break;
}
?>

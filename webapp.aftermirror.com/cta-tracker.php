<?php
if (isset($_GET["debug"])) {
	error_reporting(E_ALL);
	ini_set("display_errors", 1);
}
// standalone CTA tracker (LG G watch/android wear optimized)
// using a modified AM theme base, with aw in mind.

define("API_KEY", trim(file_get_contents("bus-api.key")));

function xml2array($url) {
	$xml = simplexml_load_string(file_get_contents($url));
	$json = json_encode($xml);
	return json_decode($json, true);
}
function time_since($since, $short = false) {
	$chunks = array(
		array(60 * 60 * 24 * 365 , 'year'),
		array(60 * 60 * 24 * 30 , 'month'),
		array(60 * 60 * 24 * 7, 'week'),
		array(60 * 60 * 24 , 'day'),
		array(60 * 60 , 'hour'),
		array(60 , 'minute'),
		array(1 , 'second')
	);

    for ($i = 0, $j = count($chunks); $i < $j; $i++) {
        $seconds = $chunks[$i][0];
        $name = $chunks[$i][1];
        if (($count = floor($since / $seconds)) != 0) {
            break;
        }
    }

    return ($count == 1) ? '1 '.$name : "{$count} {$name}s";
}

echo "
<!DOCTYPE html>
<html>
<head>
	<title>CTA Tracker</title>
	<style>
		* { font-family: 'Trebuchet MS', sans-serif; margin: 0; padding: 0; }
		body { color: white; background-color: black; }

		#content { width: 180px; overflow: hidden; position: relative; }

		h1 { font-size: 18px; float: left; }
		h2 { font-size: 12px; float: right; line-height: 18px; }
		h3 { font-size: 24px; }
		a, a:visited { color: white; }
		a.button { display: block; width: 100%; line-height: 30px; font-size: 18px; background-color: #EFEFEF; color: black; text-align: center; text-decoration: none; margin-bottom: 5px; }
	</style>
	<meta name='viewport' content='width=180, initial-scale=1, maximum-scale=1, minimum-scale=1'>
</head>
<body>
	<div id='content2'>
		<h1><a href='?'>CTA Tracker</a></h1>
		<h2>v0.1b - test</h2>
		<br clear='all' />
		<br/>
";
$do = "Home";
if (isset($_GET["do"])) $do = $_GET["do"];

switch ($do) {
	case "ViewTimes":
		$url = "http://www.ctabustracker.com/bustime/api/v1/getpredictions?key=" . API_KEY . "&stpid={$_GET['stop']}";
		$times = xml2array($url);
		if (!isset($times["prd"][0]) || !is_array($times["prd"][0])) {
			$times["prd"] = array($times["prd"]);
		}

		foreach ($times["prd"] as $time) {
			$tso = date_create_from_format("Ymd H:i", $time["prdtm"]);
			$ts = time_since($tso->format("U") - time());
			if (substr($ts, 0, 1) == "-") {
				$ts = "due";
			}
			echo "<h3>{$ts}</h3>";
			echo "<div style='font-size: 12px;'>";
			if (isset($time["dly"])) {
				echo "<b style='color: red;'>delayed</b><br/>";
			}
			echo "<b>Route:</b> {$time['rt']}<br/>";
			echo "<b>Stop:</b> {$time['stpnm']}<br/>";
			echo "<b>Bus ID:</b> {$time['vid']}<br/>";
			echo "<b>Distance:</b> {$time['dstp']} feet<br/>";
			echo "<b>Direction:</b> {$time['rtdir']}<br/>";
			echo "<b>Destination:</b> {$time['des']}<br/>";
			echo "</div>";
			echo "<br/>";
		}
	break;
	case "ViewStop":
		$url = "http://www.ctabustracker.com/bustime/api/v1/getstops?key=" . API_KEY . "&rt={$_GET['bus']}&dir={$_GET['dir']}";
		$stops = xml2array($url);
		foreach ($stops["stop"] as $data) {
			echo "<a class='button' href='?do=ViewTimes&bus={$_GET['bus']}&dir={$_GET['dir']}&stop={$data['stpid']}' style='font-size: 12px;'>{$data['stpnm']}</a>";
		}
	break;
	case "ViewDirection":
		$url = "http://www.ctabustracker.com/bustime/api/v1/getdirections?key=" . API_KEY . "&rt={$_GET['bus']}";
		$directions = xml2array($url);
		foreach ($directions["dir"] as $dir) {
			//$dirsafe = strtr($dir, array(" " => "%20"));
			echo "<a class='button' href='?do=ViewStop&bus={$_GET['bus']}&dir={$dir}' style='font-size: 12px;'>{$dir}</a>";
		}
	break;
	case "ViewBus":
		$url = "http://www.ctabustracker.com/bustime/api/v1/getroutes?key=" . API_KEY;
		$routes = xml2array($url);
		foreach ($routes["route"] as $data) {
			echo "<a class='button' href='?do=ViewDirection&bus={$data['rt']}' style='font-size: 12px;'><b>{$data['rt']}</b>: {$data['rtnm']}</a>";
		}
	break;
	case "Home":
	default:
		echo "<a class='button' href='?do=ViewBus'>Buses</a>";
		echo "<a class='button' href='?do=ViewTrain'>Trains</a>";
	break;
	case "ViewTrain":
	break;
	case "TrainTest":
		switch ($_GET["op"]) {
			case 1:
			break;
			case 2:
			break;
			case 3:
			break;
		}
	break;
}
echo "
	</div>
</body>
</html>
";
?>
<?php
define("ENGINE_PLUGIN_DIR", "../plugins/");
define("BIN_DIRECTORY", "bin/");
include("../engine.php");

if (file_exists("../db/local.key")) {
	define("UX_LOCAL", true);
}
else {
	define("UX_LOCAL", false);
}

if (UX_LOCAL) {
	define("SRV_PREFIX", "entity.");
}
else {
	define("SRV_PREFIX", "");
}

if (isset($_GET["key"])) {
	$key = $_GET["key"];
	$sendify = new Sendify();
	if ($sendify->keyExists($key)) {
		if (isset($_GET["raw"])) {
			header("Content-Type: " . $sendify->getMimetype($key));
			header('Content-Disposition: inline; filename="' . $sendify->getOriginal($key) . '"');
			readfile("../webpack.ninja/data/sendify/{$key}.dat");
		}
		elseif (isset($_GET["download"])) {
			$file = "../webpack.ninja/data/sendify/{$key}.dat";
			header("Content-length: " . filesize($file));
			header('Content-Type: application/octet-stream');
			switch ($sendify->getType($key)) {
				case "image":
				case "audio":
				case "video":
				case "file":
					header('Content-Disposition: attachment; filename="' . $sendify->getOriginal($key) . '"');
				break;
			}
			readfile($file);
		}
		elseif (isset($_GET["rawPDF"])) {
			$file = "../webpack.ninja/data/sendify/{$key}.dat";
			$im = new imagick("{$file}[0]");
			$im->setImageFormat("jpg");
			header("Content-Type: image/jpeg");
			echo $im;
		}
		else {
			if ($sendify->getType($key) !== "url") {
				echo "
					<!DOCTYPE html>
					<html>
					<head>
					<title>sendify.me/{$key}</title>
					<style type='text/css'>
					img, .fancy { border: solid 1px #333; box-shadow: 0px 0px 6px #666; max-width: 100%; max-height: 100%; width: auto; height: auto; margin: auto; }
					pre { font-family: 'Lucida Console', Monaco, monospace }
					</style>
					<link href='//" . SRV_PREFIX . "aftermirror.com/static/style.css' rel='stylesheet' type='text/css'>
					<link href='//fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
					<meta name='viewport' content='width=360, initial-scale=1, maximum-scale=1, minimum-scale=1'>
					</head>
					<body>
					<div id='content'>
						<div class='background' style='height: 100px;'></div>
						<h1 class='header'><a href='/'><em>sendify.me/</em>{$key}</a></h1>
						<div id='navigation'>
							<a href='/'>shorten your own link</a>
							<a href='?raw'>raw</a>
							<a href='?download'>download</a>
						</div>
				";
			}
			$original = $sendify->getOriginal($key);
			switch ($sendify->getType($key)) {
				case "url":
					header("Location: " . file_get_contents("../webpack.ninja/data/sendify/{$key}.url"));
				break;
				case "image":
					echo "
						<h3>{$original}</h3>
						<br/>
						<div style='text-align: center;'>
							<img src='?raw' />
						</div>
					";
				break;
				case "audio":
					echo "
						<h3>{$original}</h3>
						<br/>
						<div style='text-align: center; padding: 20px;'>
							<audio controls style='width: 75%;'>
								<source src='?raw' />
							</audio>
						</div>
					";
				break;
				case "video":
					echo "
						<h3>{$original}</h3>
						<br/>
						<div style='text-align: center;'>
							<video controls style='width: 75%;' class='fancy'>
								<source src='?raw' />
							</video>
						</div>
					";
				break;
				case "file":
					echo "
						<h3>{$original}</h3>
						<br/>
						<div style='word-wrap: break-word; padding: 10px;' class='fancy'>
					";
					if (fext($original) == "pdf") {
						echo "
							<p>first page preview</p>
							<br/>
							<div style='text-align: center;'>
								<img src='?rawPDF' />
							</div>
						";
					}
					elseif (fextIsText($original) && filesize("../webpack.ninja/data/sendify/{$key}.dat") < 1024 * 1024) {
						echo "<pre>";
						echo htmlentities(file_get_contents("../webpack.ninja/data/sendify/{$key}.dat"));
						echo "</pre>";
					}
					else {
						echo "<h3>no file preview available</h3>";
					}
					echo "
						</div>
					";
				break;
			}
			echo "
				</div>
				</body>
				</html>
			";
		}
	}
	else {
		header("Location: //" . SRV_PREFIX . "aftermirror.com/app.Sendify");
	}
}
else {
	header("Location: //" . SRV_PREFIX . "aftermirror.com/app.Sendify");
}
?>

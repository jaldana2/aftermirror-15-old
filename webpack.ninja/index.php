<?php
define("ENGINE_PLUGIN_DIR", "../plugins/");
define("BIN_DIRECTORY", "bin/");
include("../engine.php");

if (file_exists("../db/local.key")) {
	define("UX_LOCAL", true);
}
else {
	define("UX_LOCAL", false);
}

if (UX_LOCAL) {
	define("SRV_PREFIX", "entity.");
}
else {
	define("SRV_PREFIX", "");
}

header("Location: //" . SRV_PREFIX . "aftermirror.com/app.Webpack");
?>

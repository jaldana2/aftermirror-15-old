<?php
set_time_limit(0);
include("engine.php");
include("ua-tools.php");
if (file_exists(ROOT_DIR . "ua-running.flag")) {
	die("ua-running.flag is set. Not running.");
}
file_put_contents(ROOT_DIR . "ua-running.flag", time());
// super messy anime downloader v2

// tree
/*
	pseudo
		TORRENT_TITLE
			STANDARD_TITLE (or TRUE if newly added)
	anime
		TITLE
			EPISODE
				1080p => T/F
				720p => T/F
				360p => T/F
*/
				
// check for new
$channel = $xml->getElementsByTagName("channel")->item(0);
$queue = array();
foreach ($channel->getElementsByTagName("item") as $item) {
	$file = $item->getElementsByTagName("title")->item(0)->nodeValue;
	if (strc($file, " - Batch")) continue; // multiple not supported
	$dc = decouple($file);
	if (isset($db["pseudo"][$dc["anime"]])) {
		if ($db["pseudo"][$dc["anime"]] === true) {
			echo "{$dc['anime']} set, but pseudo not set.<br/>\n";
		}
		else {
			echo "{$dc['anime']} set, parsing.<br/>\n";
			$anime = $db["pseudo"][$dc["anime"]];
			if (isset($db["anime"][$anime][$dc["episode"]])) {
				echo "[Episode exists]<br/>\n";
				continue;
			}
			else {
				if (isset($queue[$anime][$dc["episode"]])) {
					if (!isHigherQuality($dc["quality"], $queue[$anime][$dc["episode"]]["quality"])) {
						echo "-- Quality is equal/lower, skip.<br/>\n";
						continue;
					}
					else {
						echo "-- We already have this episode, but this is a higher quality.<br/>\n";
					}
				}
				// enqueue
				$link = $item->getElementsByTagName("link")->item(0)->nodeValue;
				echo "-- Downloading episode {$dc['episode']} torrent data.<br/>\n";
				$queue[$anime][$dc["episode"]] = array("quality" => $dc["quality"], "link" => $link);
			}
		}
	}
	else {
		echo "{$dc['anime']} not set, ignoring.<br/>\n";
	}
}

print_a($queue);
foreach ($queue as $anime => $episodes) {
	foreach ($episodes as $episode => $data) {
		$db["anime"][$anime][$episode] = false;
		echo "Downloading .torrent: {$data['link']}<br/>\n";
		file_put_contents(ROOT_DIR . "autoadd/" . uniqid() . ".torrent", file_get_contents($data["link"]));
	}
}
writeDB(DB, $db);

// check for downloads
$dir = dir_get(ROOT_DIR . "downloaded");
foreach ($dir as $file) {
	if (is_dir($file)) continue;
	$bn = basename($file);
	if (fext($file) !== "mp4") continue;
	$dc = decouple($bn);
	if (disk_free_space(ROOT_DIR) < filesize($file) * 3) {
		die("Low disk space. Stopping. (ua-running.flag not deleted)");
	}

	$anime = $db["pseudo"][$dc["anime"]];
	if (!isset($anime) || strlen($anime) < 1) continue;
	$out = ROOT_DIR . "media/%s/%s";
	//$filesafe = strtr($file, array(" " => "\\ ", "[" => "\\[", "]" => "\\]"));
	// change $bn to standard
	$bn = "{$anime}-{$dc['episode']}.mp4";
	$filesafe = escapeshellarg($file);

	// mp4 (mkv not supported)
	// code for softsub:
	// $q = "ffmpeg -i {$filesafe} -y -strict -2 -vf \"scale=-1:1080, scale=trunc(iw/2)*2:1080, subtitles={$filesafe}\" \"{$out2}\"";

	// 720p => 360p
	$out2 = sprintf($out, "HD", $bn);
	echo "[Copying file]<br/>\n";
	copy($file, $out2);

	$out2 = sprintf($out, "SD", $bn);
	$out2safe = escapeshellarg($out2);
	$q = "ffmpeg -i {$filesafe} -y -strict -2 -vf \"scale=-1:360, scale=trunc(iw/2)*2:360\" {$out2safe}";
	echo "[Convert from 720p to 360p]<br/>\n";
	echo $q . "<br/>\n";
	exec($q);
	$db["anime"][$anime][$dc["episode"]] = array("1080p" => false, "720p" => true, "360p" => true);

	unlink($file);
	writeDB(DB, $db);
}

//exec("rsync --remove-source-files -arvz converted/ root@aftermirror.com:/var/www/urusai.ninja/media/");
unlink(ROOT_DIR . "ua-running.flag");
?>

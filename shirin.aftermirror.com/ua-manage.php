<?php
// auto-downloader macro (main)
include("engine.php");
include("ua-tools.php");

if (file_exists(ROOT_DIR . "db/local.db")) {
	define("SRV_HOST", "entity.aftermirror.com");
	define("SRV_PREFIX", "entity");
}
else {
	define("SRV_HOST", "aftermirror.com");
	define("SRV_PREFIX", "");
}

define("DL_SEARCH", "http://www.nyaa.se/?page=search&term=%s");

echo "
<!DOCTYPE html>
<html>
<head>
	<title>after|mirror: cr-ctrl</title>
	<link href='//" . SRV_HOST . "/static/style.css' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
	<meta name='viewport' content='width=360, initial-scale=1, maximum-scale=1, minimum-scale=1'>
	<link href='https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css' rel='stylesheet' type='text/css'>
	<script src='//code.jquery.com/jquery-2.1.3.min.js'></script>
	<script>
		
	</script>
</head>
<body>
	<div id='content'>
		<div class='background' style='height: 100px;'></div>
		<h1 class='header'><a href='/'><em>cr</em>-ctrl</a></h1>
		<div id='navigation'>
			<a href='/' class='current'><span class='fa fa-home'></span></a>
			<a href='?app=Manage'>manage db</a>
		</div>
";

$app = "Home";
if (isset($_GET["app"])) $app = $_GET["app"];

switch ($app) {
	case "Manage":
		echo "
			<a href='?app=Manage-AddKW'>Add keyword</a><br/>
			<a href='?app=Manage-ModPseudo'>Configure Pseudo</a><br/>
			<a href='?app=Manage-KWP'>KW-Pseudo editor</a><br/>
			<a href='?app=Manage-StatDB'>Manage stat.db</a><br/>
			<a href='?app=Manage2'>Show Advanced Options ++</a><br/>
		";
	break;
	case "Manage2":
		echo "
			<b>Use with caution:</b><br/>
			<a href='?app=HardReset-DL'>Hard Reset Downloaded</a><br/>
		";
	break;
	case "Manage-AddKW":
		$channel = $xml->getElementsByTagName("channel")->item(0);
		$anime = array();
		foreach ($channel->getElementsByTagName("item") as $item) {
			$file = $item->getElementsByTagName("title")->item(0)->nodeValue;
			$dc = decouple($file);
			$anime[$dc["anime"]][$dc["episode"]] = true;
		}
		foreach (dir_get(ROOT_DIR . "downloaded") as $file) {
			$file = basename($file);
			$dc = decouple($file);
			$anime[$dc["anime"]][$dc["episode"]] = true;
		}
		knatsort($anime);
		echo "<form action='?app=Manage-AddKW-Proc' method='post'>";
		foreach ($anime as $title => $episodes) {
			$safeTitle = base64_encode($title);
			if (isset($db["pseudo"][$title])) {
				$animeID = $db["pseudo"][$title];
				echo "(added) {$title}<br/>";
			}
			else {
				echo "<input type='checkbox' name='newanime[]' value='{$safeTitle}' /> (new) {$title}<br/>";
			}
		}
		echo "<input type='submit' />";
		echo "</form>";
	break;
	case "Manage-AddKW-Proc":
		print_a($_POST);
		foreach ($_POST["newanime"] as $title) {
			$db["pseudo"][base64_decode($title)] = true;
		}
		writeDB(DB, $db);
		echo "<h3>Database written successfully.</h3>";
	break;
	case "Manage-ModPseudo":
		foreach ($db["pseudo"] as $title => $data) {
			$safeTitle = base64_encode($title);
			if ($data === true) {
				echo "<a style='color: magenta;' href='?app=Manage-ModPseudo-Dialog&title={$safeTitle}'>{$title} (not set)</a><br/>";
			}
			else {
				echo "<a href='?app=Manage-ModPseudo-Dialog&title={$safeTitle}'>{$title} ({$data})</a><br/>";
			}
		}
	break;
	case "Manage-ModPseudo-Dialog":
		$title = base64_decode($_GET["title"]);
		echo "<b>Enter a standard title for {$title}.</b><br/>";
		echo "
			<form action='?app=Manage-ModPseudo-Proc' method='post' class='login'>
				<input type='hidden' name='title' value='{$_GET['title']}' />
				<input type='text' name='stitle' value='{$title}' />
				<input type='submit' />
			</form>
		";
	break;
	case "Manage-ModPseudo-Proc":
		$db["pseudo"][base64_decode($_POST["title"])] = $_POST["stitle"];
		writeDB(DB, $db);
		echo "<h3>Database written successfully.</h3>";
	break;
	case "HardReset-DL":
		$db["anime"] = array();
		copy(DB, DB . time() . ".bak");
		writeDB(DB, $db);
		echo "<h3>Database reset successfully.</h3>";
	break;
	case "debug":
		echo "Free space: " . disk_free_space(ROOT_DIR) . "<br/>";
		print_a($db);
	break;
	case "hardfix":
		$db2 = array();
		foreach ($db["anime"] as $anime => $data) {
			if (isSomething($anime) && strlen($anime) > 1) {
				$db2[$anime] = $data;
			}
		}
		$db["anime"] = $db2;
		copy(DB, DB . time() . ".bak");
		writeDB(DB, $db);
	break;
	case "legacy2ua":
		$old = readDB("../urusai.ninja/epi.db");
		foreach ($old as $anime => $episodes) {
			if ($anime == "__/status") continue;
			if (isset($db["pseudo"][$anime])) {
				$uanime = $db["pseudo"][$anime];
				echo "<h3>{$uanime} will be converted.</h3>";
				foreach ($episodes as $episode => $hash) {
					$db["anime"][$uanime][$episode]["1080p"] = false;

					$high = "../webpack.ninja/anime/high/{$hash}.mp4";
					if (file_exists($high) && filesize($high) > 1024 * 1024) {
						$uhigh = "media/HD/{$uanime}-{$episode}.mp4";
						if (!file_exists($uhigh)) {
							rename($high, $uhigh);
							$db["anime"][$uanime][$episode]["720p"] = true;
						}
						echo "<b>Episode {$episode} in HD will be transferred.</b><br/>";
					}
					else {
						echo "Episode {$episode} is set, but HD files does not exist.<br/>";
					}

					$low = "../webpack.ninja/anime/low/{$hash}.mp4";
					if (file_exists($low) && filesize($low) > 1024 * 1024) {
						$ulow = "media/SD/{$uanime}-{$episode}.mp4";
						if (!file_exists($ulow)) {
							rename($low, $ulow);
							$db["anime"][$uanime][$episode]["360p"] = true;
						}
						echo "<b>Episode {$episode} in SD will be transferred.</b><br/>";
					}
					else {
						echo "Episode {$episode} is set, but SD files does not exist.<br/>";
					}
				}
			}
			else {
				echo "{$anime} is not defined.<br/>";
			}
			echo "<h3>DB converted successfully.</h3>";
			copy(DB, DB . time() . ".bak");
			writeDB(DB, $db);
		}
	break;
	case "Manage-StatDB":
		$statDB = readDB(ROOT_DIR . "db/ua-anime-stat.db");
		echo "
			<style>
				tr:hover { background-color: #EFEFEF; }
			</style>
			<form action='?app=Manage-StatDB-Proc' method='post' style='font-size: 10px;'>
			<table>
				<tr>
					<th>Anime</th>
					<th>Airing?</th>
					<th>Available?</th>
					<th>Featured?</th>
					<th>Schedule</th>
					<th>Total Episodes</th>
					<th>Rating</th>
					<th>Alternate Title(s)</th>
					<th>Media Type</th>
				</tr>
		";
		$animeList = array_unique(array_values($db["pseudo"]));
		natsort($animeList);
		foreach($animeList as $anime) {
			$shard = base64_encode($anime);
			$airing = "";
			$available = "";
			$featured = "";
			$schedule = "";
			$total = "";
			$rating = "";
			$alts = "";
			$mtype = "";
			if (isset($statDB["airing"][$anime])) {
				if ($statDB["airing"][$anime]) $airing = "checked ";
			}
			if (isset($statDB["available"][$anime])) {
				if ($statDB["available"][$anime]) $available = "checked ";
			}
			if (isset($statDB["featured"][$anime])) {
				if ($statDB["featured"][$anime]) $featured = "checked ";
			}
			if (isset($statDB["schedule"][$anime]))
				$schedule = $statDB["schedule"][$anime];
			if (isset($statDB["total"][$anime]))
				$total = $statDB["total"][$anime];
			if (isset($statDB["rating"][$anime]))
				$rating = $statDB["rating"][$anime];
			if (isset($statDB["alts"][$anime]))
				$alts = $statDB["alts"][$anime];
			if (isset($statDB["mtype"][$anime]))
				$mtype = $statDB["mtype"][$anime];
			echo "
				<tr>
					<td>{$anime}</td>
					<td><input type='checkbox' name=\"airing[{$shard}]\" {$airing}/></td>
					<td><input type='checkbox' name=\"available[{$shard}]\" {$available}/></td>
					<td><input type='checkbox' name=\"featured[{$shard}]\" {$featured}/></td>
					<td><input type='text' style='width: 80px;' name=\"schedule[{$shard}]\" value='{$schedule}' /></td>
					<td><input type='text' style='width: 80px;' name=\"total[{$shard}]\" value='{$total}' /></td>
					<td><input type='text' style='width: 40px;' name=\"rating[{$shard}]\" value='{$rating}' /></td>
					<td><input type='text' name=\"alts[{$shard}]\" value='{$alts}' /></td>
					<td><input type='text' name=\"mtype[{$shard}]\" value='{$mtype}' /></td>
				</tr>
			";
		}
		echo "
			<input type='submit' />
			</table>
			</form>
		";
	break;
	case "Manage-StatDB-Proc":
		$statDB = readDB(ROOT_DIR . "db/ua-anime-stat.db");
		// reset all
		$animeList = array_unique(array_values($db["pseudo"]));
		natsort($animeList);
		foreach($animeList as $anime) {
			$statDB["airing"][$anime] = false;
			$statDB["available"][$anime] = false;
			$statDB["featured"][$anime] = false;
		}
		foreach ($_POST["airing"] as $k => $v) {
			$k = base64_decode($k);
			$statDB["airing"][$k] = true;
		}
		foreach ($_POST["available"] as $k => $v) {
			$k = base64_decode($k);
			$statDB["available"][$k] = true;
		}
		foreach ($_POST["featured"] as $k => $v) {
			$k = base64_decode($k);
			$statDB["featured"][$k] = true;
		}
		foreach ($_POST["schedule"] as $k => $v) {
			$k = base64_decode($k);
			$statDB["schedule"][$k] = $v;
		}
		foreach ($_POST["total"] as $k => $v) {
			$k = base64_decode($k);
			$statDB["total"][$k] = $v;
		}
		foreach ($_POST["rating"] as $k => $v) {
			$k = base64_decode($k);
			if (!isset($statDB["rating"][$k]) || $statDB["rating"][$k] !== $v) {
				$statDB["rating"][$k] = $v;
				$statDB["lastRated"][$k] = time();
			}
		}
		foreach ($_POST["alts"] as $k => $v) {
			$k = base64_decode($k);
			$statDB["alts"][$k] = $v;
		}
		foreach ($_POST["mtype"] as $k => $v) {
			$k = base64_decode($k);
			$statDB["mtype"][$k] = $v;
		}
		echo "<h3>statDatabase written successfully.</h3>";
		rename(ROOT_DIR . "db/ua-anime-stat.db", ROOT_DIR . "db/ua-anime-stat.db." . time() . ".bak");
		writeDB(ROOT_DIR . "db/ua-anime-stat.db", $statDB);
	break;
	case "Manage-KWP":
		echo "<form action='?app=Manage-KWP-Proc' method='post'>";
		echo "<h3>pseudo</h3>";
		echo "
			<table id='pseudotable' style='width: 100%;'>
				<thead>
					<tr>
						<th>Unique Torrent ID</th>
						<th>Match String</th>
					</tr>
				</thead>
				<tbody>
		";
		foreach ($db["pseudo"] as $k => $v) {
			echo "
				<tr>
					<td><input style='width: 100%;' type='text' name='k[]' value='{$k}' /></td>
					<td><input style='width: 100%;' type='text' name='v[]' value='{$v}' /></td>
				</tr>
			";
		}
		echo "</tbody></table>";
		echo "<span onclick='addRow();'>add</span>&nbsp;&nbsp;";
		echo "<input type='submit' />";
		echo "</form>";
		echo "
			<script>
				function addRow() {
					$('#pseudotable > tbody:last').append(\"<tr><td><input style='width: 100%;' type='text' name='k[]' /></td><td><input style='width: 100%;' type='text' name='v[]' /></td></tr>\");
				}
			</script>
		";
	break;
	case "Manage-KWP-Proc":
		$pseudo = array();
		$total = count($_POST["k"]);
		for ($i = 0; $i < $total; $i++) {
			if (isSomething($_POST["k"][$i]) && isSomething($_POST["v"][$i])) {
				$pseudo[$_POST["k"][$i]] = $_POST["v"][$i];
			}
		}
		$db["pseudo"] = $pseudo;
		//copy(DB, DB . time() . ".bak");
		writeDB(DB, $db);
		echo "<h3>Pseudo set successfully.</h3>";
	break;
	default:

	break;
}

if (file_exists("ua-running.flag")) {
	echo "<h3>ua-running.flag exists! changes to ua-anime.db (kw and pseudo) may not be saved</h3>";
}

echo "
		<div id='footer'>
			<a href='//" . SRV_HOST . "'>after|mirror</a>
		</div>
	</div>
</body>
</html>
";
?>

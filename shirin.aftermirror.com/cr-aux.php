<?php
// auto-downloader macro (aux) [spectrum]
include("engine.php");

// Tasks:
/*
	- parse nyaa for new torrents (720p-1080p)
	- conversion tree(s):
		1080p
		720p
		360p
	- upload to main
*/

// Proposed DB struct
/*
	table: media
		media_id INTEGER PRIMARY KEY,
		anime_id STRING,
		episode INTEGER,
		size INTEGER,
		status STRING,
		quality STRING,
		media_hash STRING,
		date_added INTEGER,
		date_completed INTEGER
	table: anime
		anime_id INTEGER PRIMARY KEY,
		title STRING,
		alt_titles STRING,
		status STRING,
		episode_total INTEGER,
		episode_current INTEGER,
		last_update INTEGER,
		
*/

define("XM_ENABLED", true);
if (!XM_ENABLED) die("XM (aux-macro) is not enabled");

define("DL_SEARCH", "http://www.nyaa.se/?page=search&term=%s");

$db = new SQLite3("db/cr-anime.db");


?>

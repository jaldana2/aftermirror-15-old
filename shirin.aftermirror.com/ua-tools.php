<?php
define("ROOT_DIR", "/var/www/shirin.aftermirror.com/");
//define("NYAA_URL", "http://www.nyaa.se/?page=rss&term=bakedfish%7Cdeadfish%7Chorriblesubs");
//define("NYAA_URL", "http://www.nyaa.se/?page=rss&term=horriblesubs");
define("NYAA_URL", "http://www.nyaa.se/?page=rss&term=bakedfish%7Cdeadfish%20720p");

// this is strangely harder than it looks
function decouple($file) {
	$r = array();

	// file extension
	$r["fext"] = fext($file);

	// group
	sscanf($file, "%s ", $group);
	$r["group"] = trim($group, "[] ");

	// let's get rid of some stuff.
	$file = substr($file, strlen($r["group"]) + 3, -(strlen($r["fext"]) + 1));

	switch($r["group"]) {
		case "BakedFish":
		case "DeadFish":
			// Shokugeki no Souma - 08 [720p][AAC]
			// Genshiken Nidaime - 03 - Special [BD][720p][AAC]
			// Sekai Seifuku: Bouryaku no Zvezda - Shin Zvezda Daisakusen - 13 - Special [BD][1080p][AAC]
			$file = strtr($file, array("[AAC]" => "", "[BD]" => "", "[DVD]" => "", "ONA " => ""));
		case "HorribleSubs":
		default:
			// Shokugeki no Souma - 08 [720p]
			// Genshiken Nidaime - 03 - Special [720p]
			// Sekai Seifuku: Bouryaku no Zvezda - Shin Zvezda Daisakusen - 13 - Special [1080p]
			$quality = substr($file, strripos($file, "["));
			$quality = trim($quality, "[] ");
			$r["quality"] = $quality;
			$file = trim(substr($file, 0, -(strlen($quality) + 3)), "[] ");
			// Shokugeki no Souma - 08
			// Genshiken Nidaime - 03 - Special
			// Sekai Seifuku: Bouryaku no Zvezda - Shin Zvezda Daisakusen - 13 - Special
			$file = strtr($file, array(" - Special" => "sp"));
			// Shokugeki no Souma - 08
			// Genshiken Nidaime - 03sp
			// Sekai Seifuku: Bouryaku no Zvezda - Shin Zvezda Daisakusen - 13sp
			$title = substr($file, 0, strripos($file, " - "));
			$r["anime"] = $title;
			$episode = substr($file, strlen($title) + 3);
			$episode = strtr($episode, array("v0" => "", "v1" => "", "v2" => "", "v3" => "", "v4" => "", "v5" => "", "v6" => ""));
			if (strc($episode, "sp")) {
				$r["special"] = "yes";
				$r["episode"] = substr($episode, 0, -2);
			}
			else {
				$r["special"] = "no";
				$r["episode"] = $episode;
			}
			$r["episode"] = trim($r["episode"], "[] ");
		break;
	}

	return $r;
}
function isHigherQuality($this, $exist) {
	$a = qton($this);
	$b = qton($exist);
	if ($a > $b) return true;
	return false;
}
function qton($quality) {
	$a = 0;
	switch($quality) {
		case "240p":
		case "360p":
		$a = 1;
		break;
		case "480p":
		case "540p":
		$a = 2;
		break;
		case "700p":
		case "720p":
		$a = 3;
		break;
		case "960p":
		$a = 4;
		break;
		case "1080p":
		$a = 5;
		break;
	}
	return $a;
}

define("DB", ROOT_DIR . "db/ua-anime.db");
$db = readDB(DB);

// get new
$xml = new DOMDocument();
$xml->load(NYAA_URL);
?>
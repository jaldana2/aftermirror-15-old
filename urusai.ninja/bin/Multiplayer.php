<?php
if (isset($_GET["invite"])) $invite = $_GET["invite"];
if (isset($_GET["video"])) $video = $_GET["video"];
if (isset($_GET["title"])) $title = $_GET["title"];
$host = $_SESSION["authUser"];
if (isset($_GET["join"])) { $uniqid = $_GET["join"]; }
else { $uniqid = substr(md5(uniqid()), 5, 5); }

echo "
<h3>{$title}</h3>
<a href='?video={$video}&title={$title}&join={$uniqid}' id='inviteLink'>Room {$uniqid}</a>
<button id='inviteLinkD' data-clipboard-text='https://urusai.ninja/app.Multiplayer?video={$video}&title={$title}&join={$uniqid}' title='Click to copy the invite URL'>Copy to Clipboard</button>
    
<script src='//" . SRV_PREFIX . "aftermirror.com/static/ZeroClipboard.min.js'></script>
<script>
$(function() {
	var zeroClient = new ZeroClipboard(document.getElementById('inviteLinkD'));
	$('#inviteLink').on('click', function(e) {
		e.preventDefault();
		
		// Experimental copy paste stuff using Clipboard API
		var copyEvent = new ClipboardEvent('copy', { dataType: 'text/plain', data: 'https://urusai.ninja/app.Multiplayer?video={$video}&title={$title}&join={$uniqid}' });
		document.dispatchEvent(copyEvent);
		return false;
	});
	
	zeroClient.on( 'ready', function( readyEvent ) {
	  // alert( 'ZeroClipboard SWF is ready!' );

	  zeroClient.on( 'aftercopy', function( event ) {
		//event.target.style.display = 'none';
		//alert('Copied text to clipboard: ' + event.data['text/plain'] );
		alert('Invite URL copied!');
	  } );
	} );
});
</script>
<br/>
<br/>
<script src='//" . SRV_PREFIX . "aftermirror.com/static/stream.js'></script>
<style type='text/css'>
#ctlMessageBoxCtr {
	width: 100%;
	height: 200px;
	border: solid 1px black;
	box-shadow: 0px 0px 4px black;
	overflow: hidden;
}
#ctlMessageBox {
	height: 100%;
	padding: 5px 10px;
	font-size: 12px;
	overflow-x: hide;
	overflow-y: scroll;
	background-color: #303030;
	color: white;
	text-shadow: none;
	margin-right: -15px;
}
#ctlMessageBox div.ctlPost {
	word-wrap: break-word;
}
#ctlMessageBox div.ctlPost b {
	color: #87C3E8;
}
#ctlChatOverlay {
	background-color: rgba(0, 0, 0, 0.5);
	text-shadow: none;
	color: white;
	width: 100%;
	display: inline-block;
	text-align: right;
	overflow: hidden;
	padding: 0px 0px;
	position: absolute;
	z-index: 9999;
}
#ctlChatOverlay span.ctlChatOverlayPost {
	
}
#ctlChatOverlay span.ctlChatOverlayPost b {
	color: #87C3E8;
}
#ctlChatOverlay span.ctlChatOverlayPost span {
	font-weight: bold;
}

.ctlPicon {
	width: 28px;
	height: 28px;
	background-size: cover;
	background-position: center center;
	background-repeat: no-repeat;
	display: inline-block;
	margin-right: 4px;
}


input[type=range].slidey {
  -webkit-appearance: none;
  margin: 7.5px 0;
}
input[type=range].slidey:focus {
  outline: none;
}
input[type=range].slidey::-webkit-slider-runnable-track {
  width: 100%;
  height: 5px;
  cursor: pointer;
  box-shadow: 0px 0px 0.4px #000000, 0px 0px 0px #0d0d0d;
  background: #bcbcbc;
  border-radius: 0.6px;
  border: 0px solid rgba(0, 0, 0, 0);
}
input[type=range].slidey::-webkit-slider-thumb {
  box-shadow: 1px 1px 1px #000000, 0px 0px 1px #0d0d0d;
  border: 0px solid #000000;
  height: 20px;
  width: 10px;
  border-radius: 10px;
  background: #ffffff;
  cursor: pointer;
  -webkit-appearance: none;
  margin-top: -7.5px;
}
input[type=range].slidey:focus::-webkit-slider-runnable-track {
  background: #cbcbcb;
}
input[type=range].slidey::-moz-range-track {
  width: 100%;
  height: 5px;
  cursor: pointer;
  box-shadow: 0px 0px 0.4px #000000, 0px 0px 0px #0d0d0d;
  background: #bcbcbc;
  border-radius: 0.6px;
  border: 0px solid rgba(0, 0, 0, 0);
}
input[type=range].slidey::-moz-range-thumb {
  box-shadow: 1px 1px 1px #000000, 0px 0px 1px #0d0d0d;
  border: 0px solid #000000;
  height: 20px;
  width: 10px;
  border-radius: 10px;
  background: #ffffff;
  cursor: pointer;
}
input[type=range].slidey::-ms-track {
  width: 100%;
  height: 5px;
  cursor: pointer;
  background: transparent;
  border-color: transparent;
  color: transparent;
}
input[type=range].slidey::-ms-fill-lower {
  background: #adadad;
  border: 0px solid rgba(0, 0, 0, 0);
  border-radius: 1.2px;
  box-shadow: 0px 0px 0.4px #000000, 0px 0px 0px #0d0d0d;
}
input[type=range].slidey::-ms-fill-upper {
  background: #bcbcbc;
  border: 0px solid rgba(0, 0, 0, 0);
  border-radius: 1.2px;
  box-shadow: 0px 0px 0.4px #000000, 0px 0px 0px #0d0d0d;
}
input[type=range].slidey::-ms-thumb {
  box-shadow: 1px 1px 1px #000000, 0px 0px 1px #0d0d0d;
  border: 0px solid #000000;
  height: 20px;
  width: 10px;
  border-radius: 10px;
  background: #ffffff;
  cursor: pointer;
  height: 5px;
}
input[type=range].slidey:focus::-ms-fill-lower {
  background: #bcbcbc;
}
input[type=range].slidey:focus::-ms-fill-upper {
  background: #cbcbcb;
}
</style>
<div style='width: 100%; position: relative; box-sizing: border-box;'>
<div id='ctlChatOverlay'></div>
</div>
<video id='ctlVideo' style='width: 100%; box-shadow: 0px 0px 4px black;'>
	<source src='//{$video}' type='video/mp4' />
</video>
<form id='frmChat'>
	<input type='text' id='ctlText' style='font-size: 10px; margin-top: 0px; width: 100%; padding: 1px 4px; color: black; box-sizing: border-box;' placeholder='mumble, mumble...' autocomplete='off' />
</form>
<div style='position: relative; margin-bottom: -30px; padding-top: 10;'>
	<input class='slidey' type='range' style='width: 100%;' id='ctlTime' min='0' />
	<span id='ctlTimeText' style='position: absolute; right: 0px; top: 20px; font-size: 12px; color: black;'>0:00 / 0:00</span>
</div>
<br/>
<br/>
<span class='fa fa-play' id='ctlPlayPause'></span> | 
<span class='fa fa-volume-up' id='ctlVolume'></span><!-- | 
<span class='fa fa-video-camera' id='ctlQuality'> SD</span>-->
<br/>
<br/>
<div id='ctlMessageBoxCtr'>
	<div id='ctlMessageBox'>
	</div>
</div>
<script src='//" . SRV_PREFIX . "aftermirror.com/static/socket.io-1.2.0.js'></script>
<script>
	var chime = document.createElement('audio');
	var chimeEnabled = true;

	var socket = io.connect('wss://" . SRV_PREFIX . "aftermirror.com:182/swatch');
	var video = document.getElementById('ctlVideo');
	
	$(function() {
		video = document.getElementById('ctlVideo');
		
		// set chime src
		chime.volume = 0.5;
		chime.src = '//" . SRV_PREFIX . "aftermirror.com/static/chime.mp3';
		socket.on('new user', function(e) {
			if (e.room != '{$uniqid}') return;
			if (!$('#pi_' + e.username).attr('id')) {
				$('#ctlStatus').prepend(
					$('<div>')
						.addClass('ctlPicon')
						.css('background-image', 'url(core.Data?c=profile-picture&v=' + e.username + ')')
						.attr('id', 'pi_' + e.username)
				);
			}
			if (e.role == 'host') {
				parseMsg('system', e.username + ' has created a new room');
			}
			else {
				parseMsg('system', e.username + ' has joined the room');
			}
		});
		socket.on('chat message', function(e) {
			if (e.room != '{$uniqid}') return;
			chime.play();
			if (!$('#pi_' + e.username).attr('id')) {
				$('#ctlStatus').prepend(
					$('<div>')
						.addClass('ctlPicon')
						.css('background-image', 'url(core.Data?c=profile-picture&v=' + e.username + ')')
						.attr('id', 'pi_' + e.username)
				);
			}
			parseMsg(e.username, e.message);
		});
		
		$('#ctlText').keypress(function(e) {
			if (e.which == 13) {
				var ctxt = $('#ctlText').val();
				if (ctxt == '/help') {
					parseMsg('system', '<b>Available Commands</b><br/>/help, /chimeoff, /chimeon');
				}
				else if (ctxt == '/chimeoff') {
					chimeEnabled = false;
					parseMsg('system', 'chime disabled');
				}
				else if (ctxt == '/chimeon') {
					chimeEnabled = true;
					parseMsg('system', 'chime enabled');
				}
				else if (ctxt.length > 0) {
					socket.emit('chat message', { message: $('#ctlText').val(), username: '" . $_SESSION["authUser"] . "', room: '{$uniqid}' });
				}
				$('#ctlText').val('');
				e.preventDefault();
				return false;
			}
        });
        
        // overloads
       	$('#ctlPlayPause').off('click').on('click', function() {
			if (!video.paused) {
				socket.emit('interact', { action: 'pause', room: '{$uniqid}' });
				socket.emit('chat message', { message: 'paused the episode', username: '" . $_SESSION["authUser"] . "', room: '{$uniqid}' });
			}
			else {
				socket.emit('interact', { action: 'play', room: '{$uniqid}' });
				socket.emit('chat message', { message: 'resumed the episode', username: '" . $_SESSION["authUser"] . "', room: '{$uniqid}' });
			}
		});
		$('#ctlQuality').off('click').on('click', function() {
			var time = video.currentTime;
			if (hdmode) {
				hdmode = false;
				video.src = sdroot;
				$(this).css('color', 'inherit').text(' SD');
				socket.emit('chat message', { message: 'switched to SD', username: '" . $_SESSION["authUser"] . "', room: '{$uniqid}' });
			}
			else {
				hdmode = true;
				video.src = hdroot;
				$(this).css('color', 'black').text(' HD');
				socket.emit('chat message', { message: 'switched to HD', username: '" . $_SESSION["authUser"] . "', room: '{$uniqid}' });
			}
			// pause video, and set time to correct time
			video.currentTime = time;
			socket.emit('interact', { action: 'pause', room: '{$uniqid}' });
		});
		$('#ctlTime').off('change').on('change', function() {
			socket.emit('interact', { action: 'seek', value: $(this).val(), room: '{$uniqid}' });
		});
		
		// the actual control
		socket.on('interact', function(e) {
			if (e.room != '{$uniqid}') return;
			if (e.action == 'pause') {
				video.pause();
				$('#ctlPlayPause').removeClass('fa-pause').addClass('fa-play');
			}
			else if (e.action == 'play') {
				video.play();
				$('#ctlPlayPause').removeClass('fa-play').addClass('fa-pause');
			}
			else if (e.action == 'seek') {
				video.pause();
				$('#ctlPlayPause').removeClass('fa-pause').addClass('fa-play');
				video.currentTime = e.value;
			}
		});
		
		// profile UI colors
		socket.on('status', function(e) {
			if (e.room != '{$uniqid}') return;
			if (!$('#pi_' + e.username).attr('id')) {
				$('#ctlStatus').prepend(
					$('<div>')
						.addClass('ctlPicon')
						.css('background-image', 'url(core.Data?c=profile-picture&v=' + e.username + ')')
						.attr('id', 'pi_' + e.username)
				);
			}
			if (e.effect == 'stalled') {
				$('#pi_' + e.username).css('border', 'dotted 4px red');
			}
			else if (e.effect == 'playing') {
				$('#pi_' + e.username).css('border', 'solid 4px limegreen');
			}
			else if (e.effect == 'waiting') {
				$('#pi_' + e.username).css('border', 'dotted 4px goldenrod');
			}
			else if (e.effect == 'paused') {
				$('#pi_' + e.username).css('border', 'dotted 4px goldenrod');
			}
			else if (e.effect == 'ready') {
				$('#pi_' + e.username).css('border', 'dashed 4px limegreen');
			}
		});
		$('#ctlVideo')
			.on('stalled abort error', function(e) {
				socket.emit('interact', { action: 'pause', room: '{$uniqid}' });
				socket.emit('chat message', { message: '<b>stalled, please check status</b>', username: '" . $_SESSION["authUser"] . "', room: '{$uniqid}', skipfilter: true });
				socket.emit('status', { effect: 'stalled', username: '" . $_SESSION["authUser"] . "', room: '{$uniqid}' });
				updateOverlayLoc();
			})
			.on('play playing', function(e) {
				socket.emit('status', { effect: 'playing', username: '" . $_SESSION["authUser"] . "', room: '{$uniqid}' });
				updateOverlayLoc();
			})
			.on('waiting seeking', function(e) {
				socket.emit('status', { effect: 'waiting', username: '" . $_SESSION["authUser"] . "', room: '{$uniqid}' });
				updateOverlayLoc();
			})
			.on('pause', function(e) {
				socket.emit('status', { effect: 'paused', username: '" . $_SESSION["authUser"] . "', room: '{$uniqid}' });
				updateOverlayLoc();
			})
			.on('canplay canplaythrough', function(e) {
				socket.emit('status', { effect: 'ready', username: '" . $_SESSION["authUser"] . "', room: '{$uniqid}' });
				updateOverlayLoc();
			});
		$(window).unload(function() {
			socket.emit('chat message', { message: '<b>" . $_SESSION["authUser"] . " left the room.</b>', username: 'system', room: '{$uniqid}', skipfilter: true });
		});
		updateOverlayLoc();
		setInterval('updateOverlayLoc()', 1000);
		setInterval('overlayScrubber()', 1000);
		parseMsg('system', '<b>Available Commands</b><br/>/help, /chimeoff, /chimeon');
	});
	
	function updateOverlayLoc() {
		// chat overlay
		$('#ctlChatOverlay').position({
			my: 'right top',
			at: 'right top',
			of: '#ctlVideo'
		});
	}
	function parseMsg(user, msg) {
		playChime();
		msg = msg.replace(\";Bawl\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/Bawl.gif' />\")
			.replace(\";Cam\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/Cam.gif' />\")
			.replace(\";Cell\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/Cell.gif' />\")
			.replace(\";Com\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/Com.gif' />\")
			.replace(\";Cry\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/Cry.gif' />\")
			.replace(\";Cute\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/Cute.gif' />\")
			.replace(\";Dog\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/Dog.gif' />\")
			.replace(\";Etc\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/Etc.gif' />\")
			.replace(\";Flu\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/Flu.gif' />\")
			.replace(\";Flwr\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/Flwr.gif' />\")
			.replace(\";Fury\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/Fury.gif' />\")
			.replace(\";Gift\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/Gift.gif' />\")
			.replace(\";Grin\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/Grin.gif' />\")
			.replace(\";Heat\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/Heat.gif' />\")
			.replace(\";Hmph\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/Hmph.gif' />\")
			.replace(\";Hset\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/Hset.gif' />\")
			.replace(\";Kiss\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/Kiss.gif' />\")
			.replace(\";Lol\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/Lol.gif' />\")
			.replace(\";Look\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/Look.gif' />\")
			.replace(\";Losr\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/Losr.gif' />\")
			.replace(\";Love\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/Love.gif' />\")
			.replace(\";LuvU\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/LuvU.gif' />\")
			.replace(\";Mad\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/Mad.gif' />\")
			.replace(\";Meh\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/Meh.gif' />\")
			.replace(\";Mkup\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/Mkup.gif' />\")
			.replace(\";Nml\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/Nml.gif' />\")
			.replace(\";No\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/No.gif' />\")
			.replace(\";Ouch\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/Ouch.gif' />\")
			.replace(\";Pray\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/Pray.gif' />\")
			.replace(\";Sad\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/Sad.gif' />\")
			.replace(\";Shy\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/Shy.gif' />\")
			.replace(\";Sigh\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/Sigh.gif' />\")
			.replace(\";Sly\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/Sly.gif' />\")
			.replace(\";Spit\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/Spit.gif' />\")
			.replace(\";Spy\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/Spy.gif' />\")
			.replace(\";VV\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/VV.gif' />\")
			.replace(\";Wave\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/Wave.gif' />\")
			.replace(\";What\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/What.gif' />\")
			.replace(\";Yawn\", \"<img src='//" . SRV_PREFIX . "aftermirror.com/static/audi-icons/Yawn.gif' />\");
			
		$('#ctlMessageBox').prepend(
			$('<div>')
				.addClass('ctlPost')
				.html('<b>' + user + '</b> <span>' + msg + '</span>')
		);
		$('#ctlChatOverlay').append(
			$('<span>')
				.addClass('ctlChatOverlayPost')
				.attr('timestamp', new Date().getTime() / 1000)
				.html('<span>' + msg + '</span> <b>&laquo; ' + user + '</b><br/>')
				.hide(0).fadeIn(200)
		);
	}
	function overlayScrubber() {
		var time = new Date().getTime() / 1000;
		$('span.ctlChatOverlayPost').each(function() {
			if (time - $(this).attr('timestamp') > 3) {
				$(this).hide(200).remove();
			}
		});
	}
	function playChime() {
		if (!chimeEnabled) return;
		chime.pause();
		chime.currentTime = 0;
		chime.play();
	}
";
if (isset($_GET["join"])) {
	echo "
		socket.emit('user connect', { username: '" . $_SESSION["authUser"] . "', room: '{$uniqid}', role: 'client' });
	";
}
else {
	echo "
		socket.emit('user connect', { username: '" . $_SESSION["authUser"] . "', room: '{$uniqid}', role: 'host' });
	";
}
echo "
	updateOverlayLoc();
</script>
";
?>

<?php
$video = $_GET["video"];
$title = $_GET["title"];

$epiV2DB = "../shirin.aftermirror.com/db/ua-anime.db";
$epiV2 = readDB($epiV2DB);

$anime = substr($title, 0, strripos($title, " - "));
$episode = substr($title, strlen($anime) + 3);

$bannerCode = "";
if (file_exists("banners/{$anime}.jpg")) {
	$banner = rawurlencode($anime);
	$bannerCode = " style='background: url(banners/{$banner}.jpg);'";
}

echo "
	<div class='banner featured'{$bannerCode}>
		<div class='h3_title'><h3>{$anime}</h3><small>Episode {$episode}</small></div>
";
echo "</div>";

$episodes = $epiV2["anime"][$anime];
knatsort($episodes);
if (isset($episodes)) {
	echo "<br/><div class='nav' style='margin-bottom: 20px;'><b>Episode</b>&nbsp;&nbsp;";
	$dq = "SD";
	if (strc($_GET["video"], "/HD/")) {
		$dq = "HD";
	}
	foreach ($episodes as $ep => $data) {
		if ($ep == $episode) {
			echo "<a href='app.Player?video=shirin.aftermirror.com/media/{$dq}/{$anime}-{$ep}.mp4&title={$anime}%20-%20{$ep}'><b>{$ep}</b></a>";
		}
		else {
			echo "<a href='app.Player?video=shirin.aftermirror.com/media/{$dq}/{$anime}-{$ep}.mp4&title={$anime}%20-%20{$ep}'>{$ep}</a>";
		}
	}
	echo "<br/>";
	$qualities = $epiV2["anime"][$anime][$episode];
	echo "<b>Quality</b>&nbsp;&nbsp;";
	foreach ($qualities as $q => $avail) {
		if ($q == "1080p" && $avail) {
			// technically 1080p not yet supported
			echo "<a href='app.Player?video=shirin.aftermirror.com/media/HD-1080p/{$anime}-{$episode}.mp4&title={$anime}%20-%20{$episode}'>HD 1080p</a>";
		}
		elseif ($q == "720p" && $avail) {
			if ($dq == "HD") {
				echo "<a href='app.Player?video=shirin.aftermirror.com/media/HD/{$anime}-{$episode}.mp4&title={$anime}%20-%20{$episode}'><b>HD 720p</b></a>";
			}
			else {
				echo "<a href='app.Player?video=shirin.aftermirror.com/media/HD/{$anime}-{$episode}.mp4&title={$anime}%20-%20{$episode}'>HD 720p</a>";
			}
		}
		elseif ($q == "360p" && $avail) {
			if ($dq == "SD") {
				echo "<a href='app.Player?video=shirin.aftermirror.com/media/SD/{$anime}-{$episode}.mp4&title={$anime}%20-%20{$episode}'><b>SD 360p</b></a>";
			}
			else {
				echo "<a href='app.Player?video=shirin.aftermirror.com/media/SD/{$anime}-{$episode}.mp4&title={$anime}%20-%20{$episode}'>SD 360p</a>";
			}
		}
	}
	echo "</div>";
}

echo "
<video controls style='width: 100%;'>
	<source src='//{$video}' type='video/mp4' />
</video>
<br/>
<br/>
<a class='button' href='//{$video}'>direct link</a>
<a class='button' href='app.Multiplayer?video={$video}&title={$title}'>multiplay (watch with others)</a>
";
?>

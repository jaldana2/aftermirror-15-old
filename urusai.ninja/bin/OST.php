<?php
echo "<h1>current OST listing</h1><br/>";
$epiV2DB = "../shirin.aftermirror.com/db/ua-anime.db";
$epiV2 = readDB($epiV2DB);
$mediaDBP = "../shirin.aftermirror.com/db/ua-anime-media.db";
$mediaDB = readDB($mediaDBP);
$statPath = "../shirin.aftermirror.com/db/ua-anime-stat.db";
$statDB = readDB($statPath);

/*
 @@ WARNING
 @@ Since user access levels isn't completely up yet, we're hardcoding the administration page.
*/
if (isset($_GET["mgr"]) && $_SESSION["authUser"] == "jpaldana") {
	echo "<b>OST Manager Mode</b> <a href='?mgr'>Add</a> | <a href='?mgr=Medit'>Medit</a><br/><br/>";
	$manager = "Home";
	if (isset($_GET["mgr"])) $manager = $_GET["mgr"];
	switch ($manager) {
		case "AddMedia":
			echo "
				<form action='?mgr=AddMedia-Proc' method='post' enctype='multipart/form-data' class='login'>
					<input type='hidden' name='anime' value='{$_GET['anime']}' />
					<label for='file'>File to upload</label>
					<input type='file' name='file' id='file' accept='audio/*' />
					<label for='type'>Type</label>
					<select name='type' id='type' style='width: 100%;'>
						<option value='OP'>OP (opening theme)</option>
						<option value='ED'>ED (ending theme)</option>
					</select>
					<label for='title'>Title</label>
					<input type='text' name='title' id='title' />
					<label for='artist'>Artist</label>
					<input type='text' name='artist' id='artist' />
					<label for='relstart'>Related Episodes (start)</label>
					<input type='text' name='relstart' id='relstart' />
					<label for='relend'>Related Episodes (end)</label>
					<input type='text' name='relend' id='relend' />
					<br/>
					<input type='submit' class='button' value='Submit' />
				</form>
			";
		break;
		case "AddMedia-Proc":
			$key = md5($_POST["anime"] . $_POST["type"] . $_POST["title"]);
			$target = "../webpack.ninja/data/urusai-media/{$key}.mp3";
			if (move_uploaded_file($_FILES["file"]["tmp_name"], $target)) {
				$mediaDB[$_POST["anime"]][$_POST["type"]][] = array(
						"title" => $_POST["title"],
						"artist" => $_POST["artist"],
						"start" => $_POST["relstart"],
						"end" => $_POST["relend"],
						"key" => $key,
						"available" => true
					);
				writeDB($mediaDBP, $mediaDB);
				echo "Success.";
			}
		break;
		case "Medit":
			echo "
				<style type='text/css'>
				table { font-size: 10px; }
				td { text-align: center; }
				</style>
				<form action='?mgr=Medit-Proc' method='post'>
				<table style='width: 100%;'>
				<tr>
					<th>Anime</th>
					<th>Type</th>
					<th>Title</th>
					<th>Artist</th>
					<th>Start</th>
					<th>End</th>
					<th>Key</th>
					<th>Available</th>
				</tr>
			";
			$i = 0;
			foreach ($mediaDB as $anime => $media) {
				foreach ($media as $type => $dat) {
					foreach ($dat as $data) {
						echo "
							<tr>
								<td><input type='text' name='anime[{$i}]' size='15' value='{$anime}' /></td>
								<td><input type='text' name='type[{$i}]' size='15' value='{$type}' /></td>
								<td><input type='text' name='title[{$i}]' size='15' value='{$data['title']}' /></td>
								<td><input type='text' name='artist[{$i}]' size='15' value='{$data['artist']}' /></td>
								<td><input type='text' name='start[{$i}]' size='10' value='{$data['start']}' /></td>
								<td><input type='text' name='end[{$i}]' size='10' value='{$data['end']}' /></td>
								<td><input type='text' name='key[{$i}]' size='10' value='{$data['key']}' /></td>
								<td><input type='text' name='available[{$i}]' size='6' value='{$data['available']}' /></td>
							</tr>
						";
						$i++;
					}
				}
			}
			echo "
				</table>
				<input type='submit' value='Save' />
				</form>
			";
		break;
		case "Medit-Proc":
			$new = array();
			for ($i = 0; $i < count($_POST["anime"]); $i++) {
				$new[$_POST["anime"][$i]][$_POST["type"][$i]][] = array(
						"title" => $_POST["title"][$i],
						"artist" => $_POST["artist"][$i],
						"start" => $_POST["start"][$i],
						"end" => $_POST["end"][$i],
						"key" => $_POST["key"][$i],
						"available" => $_POST["available"][$i]
					);
			}
			writeDB($mediaDBP, $new);
			echo "Saved!";
		break;
		default:
			knatsort($statDB["airing"]);
			foreach (array_keys($statDB["airing"]) as $anime) {
				echo "<h3>{$anime}</h3>";
				echo "<a href='?mgr=AddMedia&anime={$anime}'>Add Media</a><br/>";
				if (isset($mediaDB[$anime])) {
					foreach ($mediaDB[$anime] as $type => $data) {
						foreach ($data as $dat) {
							echo "<a class='mbutton' href='//webpack.ninja/data/urusai-media/{$dat['key']}.mp3' id='{$dat['key']}' onclick=\"playMedia('{$dat['key']}');\"><b>{$type} Episode {$dat['start']}-{$dat['end']}</b> {$dat['title']} - {$dat['artist']}</a><br/>";
						}
					}
				}
				echo "<hr/>";
			}
		break;
	}
	echo "<hr/><br/>";
}

echo "
<style type='text/css'>
	audio { display: inline-block; width: 100%; }
</style>
<script>
	var keyid;
	function playMedia(key) {
		$('audio').remove();
		$('#' + key).after(
			\"<audio controls id='m\" + key + \"'><source src='//webpack.ninja/data/urusai-media/\" + key + \".mp3' type='audio/mp3' /></audio>\"
		);
		$('#m' + key)[0].play();
		$('#m' + key).on('ended', function() {
			refresher();
		});
		keyid = key;
	}
	function refresher() {
		var obj = $('#m' + keyid);
		if (obj[0].paused) {
			obj.remove();
		}
	}
	$(function() {
		$('body').on('click', 'a.mbutton', function(e) {
			e.preventDefault();
		});
	});
</script>
";

knatsort($mediaDB);
foreach ($mediaDB as $anime => $media) {
	echo "<h3>{$anime}</h3>";
	foreach ($media as $type => $data) {
		foreach ($data as $dat) {
			echo "<a class='mbutton' href='//webpack.ninja/data/urusai-media/{$dat['key']}.mp3' id='{$dat['key']}' onclick=\"playMedia('{$dat['key']}');\"><b>{$type} Episode {$dat['start']}-{$dat['end']}</b> {$dat['title']} - {$dat['artist']}</a><br/>";
		}
	}
}
?>

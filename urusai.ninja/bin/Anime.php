<?php
echo "<h1>current anime listing</h1><br/>";

$epiV2DB = "../shirin.aftermirror.com/db/ua-anime.db";
$statPath = "../shirin.aftermirror.com/db/ua-anime-stat.db";
$mediaDBP = "../shirin.aftermirror.com/db/ua-anime-media.db";
$epiV2 = readDB($epiV2DB);
$statDB = readDB($statPath);
$mediaDB = readDB($mediaDBP);
clearstatcache();

knatsort($epiV2["anime"]);

echo "
	<form class='login'>
		<a name='search_query'></a>
		<label for='search_query'>Search for...</label>
		<input type='text' id='search_query' placeholder='Anime Title' />
	</form>
	<script>
		var box = $('#search_query');
		var lastButtonBackspace = false;
		var lastEventMatched = false;
		$(function() {
			box.on('keyup', function(e) {
				if (e.keyCode == 8) {
					if (!lastButtonBackspace) {
						scrollToAnchor('search_query');
						lastButtonBackspace = true;
					}
					if ($(this).val().length < 1) {
						$('#search_error').text('');
						clearSearch();
					}
				}
			});
			box.on('input propertychange paste', function() {
				lastButtonBackspace = false;
				$('#search_error').text('');
				var sticky = true;
				var matches = 0;

				if ($(this).val().length < 1) return;
				$('.banner').each(function() {
					var offset = $(this).attr('anime-title').toLowerCase().indexOf($(box).val().toLowerCase());
					var ctr = $(this).next();
					if (offset >= 0) {
						matches++;
						$(this).show(100);
						if (sticky == true) {
							sticky = $(ctr).attr('shortcut-id');
							$(ctr).show(100);
						}
					}
					else {
						$(ctr).hide(100);
						$(this).hide(100);
					}
				});
				if (matches == 0) {
					$('#search_error').text('No results found.');
					lastEventMatched = false;
				}
				else if (matches == 1) {
					if (!lastEventMatched) {
						scrollToAnchor(sticky);
					}
					lastEventMatched = true;
				}
				else {
					lastEventMatched = false;
				}
			});
			box.on('keydown', function(e) {
				if (e.keyCode == 13) {
					e.preventDefault();
					return false;
				}
			});
		});
		function clearSearch() {
			$(box).val('');
			$('.banner').each(function() {
				var ctr = $(this).next();
				$(ctr).hide();
				$(this).show();
			});
		}
	</script>
";

echo "<br/><a name='AnimeNav'></a><div class='nav'>";
foreach ($epiV2["anime"] as $anime => $episodes) {
	$shortcut = cleanANString($anime);
	echo "<a href='#{$shortcut}' onclick=\"clearSearch(); scrollToAnchor('{$shortcut}');\">{$anime}</a>";
}
echo "</div>";

echo "
	<div>
		<div style='float: left;'>
			<b>Icon Legend</b><br/>
			<ul class='fa-ul'>
				<li><i class='fa-li fa fa-lightbulb-o'></i> featured</li>
				<li><i class='fa-li fa fa-star-o'></i> rating</li>
			</ul>
		</div>
		<div style='float: right; text-align: right;'>
			<b>Sort by</b><br/>
			<a href='?'>Title</a><br/>
			<a href='?sort=rating'>Rating</a><br/>
			<a href='?sort=season'>Season</a><br/>
			<a href='?sort=latest'>Latest</a>
		</div>
		<br clear='all' />
	</div>";

//foreach ($epiV2["anime"] as $anime => $episodes) {
$animeList = array_unique(array_values($epiV2["pseudo"]));
natsort($animeList);

if (isset($_GET["sort"])) {
	$a2 = array();
	switch ($_GET["sort"]) {
		case "rating":
			foreach ($animeList as $anime) {
				$a2[$anime] = $statDB["rating"][$anime];
			}
			natsort($a2);
			$animeList = array_reverse(array_keys($a2));
		break;
		case "season":
			foreach ($animeList as $anime) {
				$sched = explode(" ", $statDB["schedule"][$anime]);
				$a2[$sched[1]][$sched[0]][] = $anime;
			}
			knatsort($a2);
			$a3 = array();
			foreach (array_reverse($a2) as $year => $seasons) {
				if (isset($seasons["Fall"])) foreach (array_reverse($seasons["Fall"]) as $anime) {
					$a3[] = $anime;
				}
				if (isset($seasons["Summer"])) foreach (array_reverse($seasons["Summer"]) as $anime) {
					$a3[] = $anime;
				}
				if (isset($seasons["Spring"])) foreach (array_reverse($seasons["Spring"]) as $anime) {
					$a3[] = $anime;
				}
				if (isset($seasons["Winter"])) foreach (array_reverse($seasons["Winter"]) as $anime) {
					$a3[] = $anime;
				}
			}
			$animeList = $a3;
		break;
		case "latest":
			$i = 0;
			foreach ($animeList as $anime) {
				$episodes = $epiV2["anime"][$anime];
				$recent = max(array_keys($episodes));
				$file = "../shirin.aftermirror.com/media/SD/{$anime}-{$recent}.mp4";
				if (file_exists($file)) {
					$mod = filemtime($file);
				}
				else {
					$mod = $i++;
				}
				$a2[$anime] = $mod;
			}
			natsort($a2);
			$animeList = array_reverse(array_keys($a2));
		break;
	}
}

foreach ($animeList as $anime) {
	$episodes = @$epiV2["anime"][$anime];
	$shortcut = cleanANString($anime);

	// rudely assume latest episode is last
	//$recent = $episodes;
	if (is_array($episodes)) $recent = max(array_keys($episodes));
	else $recent = -1;
	//$recent = $episodes;
	//$recent = array_keys(array_reverse($recent, true));
	//$recent = array_pop($recent);
	$file = "../shirin.aftermirror.com/media/SD/{$anime}-{$recent}.mp4";
	if (file_exists($file)) {
		$mod = filemtime($file);
		$time = time_since(time() - $mod);
	}
	else {
		$time = "not available";
		$mod = time() - mt_rand(1000, 1000000);
		$time = time_since(time() - $mod);
	}
	if ($time == "not available") {
		$latest_tag = "<span style='color: #885555;'><span class='fa fa-warning'></span> {$time}</span>";
	}
	elseif (strc($time, "second") || strc($time, "minute") || strc($time, "hour")) {
		$latest_tag = "<span style='color: #00FF01;'>{$time} ago</span>";
	}
	elseif (strc($time, "day")) {
		$latest_tag = "<span style='color: #FF4700;'>{$time} ago</span>";
	}
	else {
		$latest_tag = "<span style='color: white;'>{$time} ago</span>";
	}

	echo "<a name='{$shortcut}'></a>";
	$color = substr(md5(rand()), 0, 6);

	// stats
	$available = false;
	$featured = false;
	$airing = false;
	$schedule = "";
	$rating = "";
	$alts = "";
	$mtype = "";
	if (isset($statDB["available"][$anime])) {
		$available = $statDB["available"][$anime];
		$featured = $statDB["featured"][$anime];
		$airing = $statDB["airing"][$anime];
		
		if (isSomething($statDB["schedule"][$anime])) {
			if ($airing) {
				$schedule = "<span class='statSchedule'>{$statDB['schedule'][$anime]} (currently airing)</span>";
			}
			else {
				$schedule = "<span class='statSchedule'>{$statDB['schedule'][$anime]}</span>";
			}
		}
		if (isSomething($statDB["rating"][$anime])) {
			$lastRating = time_since(time() - $statDB["lastRated"][$anime]);
			$rating = "<span class='statRating'><span class='fa fa-star-o fa-fw'></span>{$statDB['rating'][$anime]}<br/><small>as of {$lastRating} ago</small></span></span>";
		}
		if (isSomething($statDB["mtype"][$anime])) $mtype = "<span class='statMediaType'>{$statDB['mtype'][$anime]}</span>";
		$alts = $statDB["alts"][$anime];

		if (isSomething($statDB["total"][$anime])) {
			if ((int) $recent === (int) $statDB["total"][$anime]) {
				$latest_tag = "<span style='color: #FF4700;'>completed</span>";
			}
			else {
				$total = (int) $statDB["total"][$anime];
				// hmm...
				$episodesLeft = (int) $total - (int) $recent;
				// time difference between episodes (1 week)
				$diffTS = 60 * 60 * 24 * 7;
				$ets = time_since($diffTS * $episodesLeft);
				$latest_tag .= "&nbsp; <span class='fa fa-clock-o'></span>&nbsp; {$ets} remaining ({$episodesLeft})";
			}
		}
	}
	if ($recent == -1) {
		$available = false;
	}

	$bannerCode = "";
	if (file_exists("banners/{$anime}.jpg")) {
		$banner = rawurlencode($anime);
		$bannerCode = " style='background: url(banners/{$banner}.jpg);'";
	}

	$airingCode = "";
	$extraTitleCode = "";
	$bannerClass = "banner notfeatured";
	if ($featured) {
		$extraTitleCode .= "&nbsp;&nbsp;<span class='fa fa-lightbulb-o'></span>";
		$bannerClass = "banner featured";
	}

	if ($available) {
		echo "
			<div class='{$bannerClass}' data-ripple-color='#{$color}' anime-title='{$anime},{$alts}'{$bannerCode} onclick=\"toggleList('{$shortcut}');\">
				<div class='h3_title'><h3>{$anime}{$extraTitleCode}</h3><small><b>E{$recent}</b>: {$latest_tag}</small></div>
		";
		echo $schedule . $rating . $mtype;
		echo "</div>";
	}
	else {
		if (file_exists("banners/{$anime}.jpg")) {
			$bannerCode = " style='background: repeating-linear-gradient(-45deg, rgba(255,255,0,0.5), rgba(255,255,0,0.5) 20px, rgba(0,0,0,0.5) 20px, rgba(0,0,0,0.5) 40px), url(banners/{$banner}.jpg);'";
		}
		else {
			$bannerCode = " style='background: repeating-linear-gradient(-45deg, rgba(255,255,0,0.5), rgba(255,255,0,0.5) 20px, rgba(0,0,0,0.5) 20px, rgba(0,0,0,0.5) 40px), url(banners/default.jpg);'";
		}
		echo "
			<div class='{$bannerClass}' data-ripple-color='#{$color}' anime-title='{$anime},{$alts}'{$bannerCode} onclick=\"alert('Sorry, {$anime} is currently not available.');\">
				<div class='h3_title'><strike><h3>{$anime}{$extraTitleCode}</h3></strike><small style='color: yellow;'>not available</small></div>
		";
		echo $schedule . $rating . $mtype;
		echo "</div>";
		continue;
	}
	knatsort($episodes);
	$episodes = array_reverse($episodes, true);

	$ost = array("OP" => array(), "ED" => array());
	if (isset($mediaDB[$anime])) {
		foreach ($mediaDB[$anime] as $type => $data) {
			foreach ($data as $dat) {
				if ($dat["start"] == "*") $dat["start"] = (int) min(array_keys($episodes));
				if ($dat["end"] == "*") $dat["end"] = $recent;
				for ($i = $dat["start"]; $i <= $dat["end"]; $i++) {
					$ost[$type][$i] = $dat["key"];
				}
			}
		}
	}

	echo "<div class='ctr' anime-title='{$anime}' shortcut-id='{$shortcut}' id='ctr_{$shortcut}' style='display: none;'>";
	foreach ($episodes as $episode => $data) {
		$file = "../shirin.aftermirror.com/media/SD/{$anime}-{$episode}.mp4";
		if (file_exists($file)) {
			$mod = filemtime($file);
			$time = time_since(time() - $mod);
		}
		else {
			$time = "not available";
		}
		//echo "Episode <b>{$episode}</b>: <a href='app.Player?video=shirin.aftermirror.com/media/HD/{$anime}-{$episode}.mp4&title={$anime}%20-%20{$episode}'>HD</a> <a href='app.Player?video=shirin.aftermirror.com/media/SD/{$anime}-{$episode}.mp4&title={$anime}%20-%20{$episode}'>SD</a> ";
		echo "
			<p class='episode_row'>Episode <b>{$episode}</b> &mdash; ";

		if ($time == "not availables") {
			echo "<span style='color: #885555;'><span class='fa fa-warning'></span> {$time}</span><br/>";
		}
		else {
			echo "
				<a class='button' href='app.Player?video=shirin.aftermirror.com/media/HD/{$anime}-{$episode}.mp4&title={$anime}%20-%20{$episode}'>HD 720p</a> 
				<a class='button' href='app.Player?video=shirin.aftermirror.com/media/SD/{$anime}-{$episode}.mp4&title={$anime}%20-%20{$episode}'>SD 360p</a>
			";
			if (isset($ost["OP"][(int) $episode])) {
				$key = $ost["OP"][(int) $episode];
				echo "<a class='mbutton button' style='color: limegreen;' id='e{$key}{$episode}' href='//webpack.ninja/data/urusai-media/{$key}.mp3' onclick=\"playMedia('e{$key}{$episode}', '{$key}');\">OP</a>&nbsp;";
			}
			if (isset($ost["ED"][(int) $episode])) {
				$key = $ost["ED"][(int) $episode];
				echo "<a class='mbutton button' style='color: limegreen;' id='e{$key}{$episode}' href='//webpack.ninja/data/urusai-media/{$key}.mp3' onclick=\"playMedia('e{$key}{$episode}', '{$key}');\">ED</a>&nbsp;";
			}
			echo " ";
			if (strc($time, "second") || strc($time, "minute") || strc($time, "hour")) {
				echo "<span style='color: #00FF01;'>{$time} ago</span>";
			}
			elseif (strc($time, "day")) {
				echo "<span style='color: #FF4700;'>{$time} ago</span>";
			}
			else {
				echo "<span style='color: #555555;'>{$time} ago</span>";
			}
			echo "<br/>";
		}
		echo "</p>";
	}
	echo "</div>";
}
echo "<div style='height: 400px;'><p id='search_error'></p></div>";

echo "
<style type='text/css'>
	audio { width: 0; height: 0; display: none; }
</style>
<script>
	var playing = false;
	var keyid;
	var kmx;
	var ktext;
	function playMedia(mx, key) {
		if ($('#m' + key)[0] != undefined) {
			refresher();
			$('audio').remove();
		}
		else {
			if (playing) {
				refresher();
				$('audio').remove();
			}
			$('audio').remove();
			ktext = $('#' + mx).text();
			$('#' + mx).after(
				\"<audio id='m\" + key + \"'><source src='//webpack.ninja/data/urusai-media/\" + key + \".mp3' type='audio/mp3' /></audio>\"
			).text('Stop');
			$('#m' + key)[0].play();
			$('#m' + key).on('ended', function() {
				refresher();
			});
			keyid = key;
			kmx = mx;
			playing = true;
		}
	}
	function refresher() {
		var obj = $('#m' + keyid);
		obj.remove();
		$('#' + kmx).text(ktext);
		playing = false;
	}
	$(function() {
		$('body').on('click', '.mbutton', function(e) {
			e.preventDefault();
			return false;
		});
	});
</script>
";

/*
echo "<br/><h3>(legacy listing below)</h3>";

$epiDB = readDB("epi.db");
foreach ($epiDB as $anime => $episodes) {
	if ($anime == "__/status") continue;
	echo "<h3>{$anime}</h3>";
	knatsort($episodes);
	$episodes = array_reverse($episodes, true);
	foreach ($episodes as $episode => $hash) {
		$time = $epiDB["__/status"][$anime][$episode];
		if ($time == "downloading") continue;
		$time = time_since(time() - $time);
		echo "Episode <b>{$episode}</b>: <a href='app.Player?video=urusai.ninja/anime/high/{$hash}.mp4&title={$anime}%20-%20{$episode}'>HD</a> <a href='app.Player?video=urusai.ninja/anime/low/{$hash}.mp4&title={$anime}%20-%20{$episode}'>SD</a> ";
		if (strc($time, "second") || strc($time, "minute") || strc($time, "hour")) {
			echo "<span style='color: #00FF01;'>{$time} ago</span><br/>";
		}
		elseif (strc($time, "day")) {
			echo "<span style='color: #FF4700;'>{$time} ago</span><br/>";
		}
		else {
			echo "<span style='color: #555555;'>{$time} ago</span><br/>";
		}
	}
	echo "<br/>";
}
*/
?>

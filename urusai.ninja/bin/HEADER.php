<!DOCTYPE html>
<html>
<head>
	<title>urusai.ninja</title>
	<link href='//<?php echo SRV_PREFIX; ?>aftermirror.com/static/style.css' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Raleway:700' rel='stylesheet' type='text/css'>
	<meta name="viewport" content="width=360, initial-scale=1, maximum-scale=1, minimum-scale=1">
	<link href='https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css' rel='stylesheet' type='text/css'>
	<script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
	<script src="//<?php echo SRV_PREFIX; ?>aftermirror.com/static/urusai.jquery-ui.min.js"></script>
	<script>
		function scrollToAnchor(aid){
		    var aTag = $("a[name='"+ aid +"']");
		    $('html,body').animate({scrollTop: aTag.offset().top},500);
			$('#ctr_' + aid).show(250);
		}
		function toggleList(id) {
			var div = $('#ctr_' + id);
			div.toggle("blind", { percent: 0 }, 250 );
		}
		$(function() {
			if ($('#back-to-top').length) {
			    var scrollTrigger = 100, // px
			        backToTop = function () {
			            var scrollTop = $(window).scrollTop();
			            if (scrollTop > scrollTrigger) {
			                $('#back-to-top').addClass('show');
			            } else {
			                $('#back-to-top').removeClass('show');
			            }
			        };
			    backToTop();
			    $(window).on('scroll', function () {
			        backToTop();
			    });
			    $('#back-to-top').on('click', function (e) {
			        e.preventDefault();
			        $('html,body').animate({
			            scrollTop: 0
			        }, 700);
			    });
			}
		});
		// ripple effect courtesy of http://codepen.io/Craigtut/pen/dIfzv
		(function (window, $) {
  
		  $(function() {
		    
		    
		    $('.ripple').on('click', function (event) {
		      event.preventDefault();
		      
		      var $div = $('<div/>'),
		          btnOffset = $(this).offset(),
		      		xPos = event.pageX - btnOffset.left,
		      		yPos = event.pageY - btnOffset.top;
		      

		      
		      $div.addClass('ripple-effect');
		      var $ripple = $(".ripple-effect");
		      
		      $ripple.css("height", $(this).height());
		      $ripple.css("width", $(this).height());
		      $div
		        .css({
		          top: yPos - ($ripple.height()/2),
		          left: xPos - ($ripple.width()/2),
		          background: $(this).data("ripple-color")
		        }) 
		        .appendTo($(this));

		      window.setTimeout(function(){
		        $div.remove();
		      }, 2000);
		    });
		    
		  });
		  
		})(window, jQuery);
	</script>
</head>
<body>
	<div id="content">
		<div class="background" style="height: 100px;"></div>
		<h1 class="header"><a href='/'><em>urusai</em>.ninja</a> <img src='//<?php echo SRV_PREFIX; ?>aftermirror.com/static/images/logo1.gif' /></h1>
		<div id="navigation">
			<a href="/" class="current"><span class='fa fa-home'></span></a>
			<a href="/app.Anime">anime</a>
			<a href="/app.OST">ost</a>
<?php
	if (!$authID) {
		echo "
			<a href=\"//" . SRV_LOGIN . "/app.Login?redirect=" . SRV_HOST . "\">login</a>
		";
	}
	else {
		printf("
			<a href=\"//" . SRV_LOGIN . "/app.Profile\">%s</a>
			<a href=\"/core.LoginHandler?do=logout\" style='color: #DD5656;'><span class='fa fa-power-off'></span></a>
		", $_SESSION["authUser"]);
	}
?>
		</div>

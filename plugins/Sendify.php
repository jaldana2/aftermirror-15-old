<?php
// after|mirror authID
define("AM_SENDIFY_DB", "../db/sendify.db");

define("SK_FILE_AVAILABLE", 100);
define("SK_FILE_LOCKED", 110);
define("SK_FILE_UNAVAILABLE", 111);
define("SK_FILE_REMOVED", 112);

class Sendify {
	private $database;
	
	function __construct() {
		$this->database = new SQLite3(AM_SENDIFY_DB) or die("FAILED to connect to database");
		$this->initDB();
	}
	
	function keyExists($key) {
		$query = sprintf("SELECT * FROM sendify WHERE key = '%s'", $key);
		$result = $this->database->query($query);
		$row = $result->fetchArray();
		if (!$row) return false;
		return true;
	}
	function randKey() {
		$fullHash = sha1(time() . AM_SENDIFY_DB);
		$hash = "";
		for ($i = 1; $i < strlen($fullHash); $i++) {
			$hash = substr($fullHash, 0, $i);
			if (!$this->keyExists($hash)) return $hash;
		}
		return $this->randKey();
	}
	function addKey($key, $type, $owner, $ip) {
		if (!$this->keyExists($key)) {
			$query = sprintf("INSERT INTO sendify (key, type, owner, timestamp, ip, hits, status) VALUES ('%s', '%s', '%s', '%d', '%s', '%d', '%d')", $key, $type, $owner, time(), $ip, 0, SK_FILE_AVAILABLE);
			$this->database->exec($query);
			return true;
		}
		else {
			return false;
		}
	}
	
	function getType($key) {
		if ($this->keyExists($key)) {
			$query = sprintf("SELECT * FROM sendify WHERE key = '%s'", $key);
			$result = $this->database->query($query);
			$row = $result->fetchArray();
			return $row["type"];
		}
		else {
			return false;
		}
	}
	function getMimetype($key) {
		if ($this->keyExists($key)) {
			$query = sprintf("SELECT * FROM sendify WHERE key = '%s'", $key);
			$result = $this->database->query($query);
			$row = $result->fetchArray();
			return $row["mimetype"];
		}
		else {
			return false;
		}
	}
	function getOriginal($key) {
		if ($this->keyExists($key)) {
			$query = sprintf("SELECT * FROM sendify WHERE key = '%s'", $key);
			$result = $this->database->query($query);
			$row = $result->fetchArray();
			return $row["original"];
		}
		else {
			return false;
		}
	}
	function setMimetype($key, $value) {
		if ($this->keyExists($key)) {
			$query = sprintf("UPDATE sendify SET mimetype='%s' WHERE key='%s'", $value, $key);
			$this->database->exec($query);
			return true;
		}
		else {
			return false;
		}
	}
	function setOriginal($key, $value) {
		if ($this->keyExists($key)) {
			$query = sprintf("UPDATE sendify SET original='%s' WHERE key='%s'", $value, $key);
			$this->database->exec($query);
			return true;
		}
		else {
			return false;
		}
	}
	
	function initDB() {
		$query = "CREATE TABLE IF NOT EXISTS sendify (
			key STRING PRIMARY KEY,
			type STRING,
			owner STRING,
			timestamp INTEGER,
			ip STRING,
			hits INTEGER,
			status INTEGER,
			password STRING,
			original STRING,
			mimetype STRING
		);";
		$this->database->exec($query);
	}
}
?>

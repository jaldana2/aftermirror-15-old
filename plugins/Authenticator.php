<?php
// after|mirror authID
define("AM_LOGIN_DB", "../db/login.db");

// 0 -> 9
define("AM_STATUS_IDLE", 0);
define("AM_STATUS_LOGGED_IN", 1);
define("AM_STATUS_NOT_VERIFIED", 2);
define("AM_STATUS_FAILED_LOGIN", 3);
define("AM_STATUS_NO_CREDENTIAL", 5);
define("AM_STATUS_NO_MATCH", 6);

// 10 -> 39
define("AM_USER_UNVERIFIED", 10);
define("AM_USER_VERIFIED", 11);
define("AM_USER_RESTRICTED", 30);
define("AM_USER_BANNED", 31);
define("AM_USER_LOCKED", 32);

// 50 -> 99
define("AM_ACCESS_REGULAR", 50);
define("AM_ACCESS_VIP", 60);
define("AM_ACCESS_MOD", 90);
define("AM_ACCESS_ADMIN", 99);

class authID {
	private $username;
	private $password;
	private $sessionKey;
	private $authPin;
	private $database;
	private $status;
	private $authMethod;
	
	/* Constructs a new authID */
	function __construct($username) {
		$this->username = $username;
		$this->password = false;
		$this->sessionKey = false;
		$this->authPin = false;
		$this->database = new SQLite3(AM_LOGIN_DB) or die("FAILED to connect to database");
		$this->status = AM_STATUS_IDLE;
		$this->authMethod = "(none)";
	}
	// setPassword: set password for verification
	function setPassword($password) {
		$this->password = $this->hashify($this->username . $password);
	}
	// setSessionKey: set sessionKey for verification
	function setSessionKey($sessionKey) {
		$this->sessionKey = $sessionKey;
	}
	// setAuthPin: set authPin for verification (optional)
	function setAuthPin($authPin) {
		$this->authPin = $authPin;
	}
	
	// getUsername: returns the current username
	function getUsername() {
		return $this->username;
	}
	// getSessionKey: returns latest sessionKey from database if logged in
	function getSessionKey() {
		if ($this->status == AM_STATUS_LOGGED_IN) {
			$query = sprintf("SELECT * FROM login WHERE username = '%s'", $this->username, $this->sessionKey);
			$result = $this->database->query($query);
			$row = $result->fetchArray();
			return $row["sessionKey"];
		}
		else {
			return false;
		}
	}
	// getStatus: returns the current authID status
	function getStatus() {
		return $this->status;
	}
	// getAuthMethod: returns the method used to authenticate the user
	function getAuthMethod() {
		return $this->authMethod;
	}
	
	// resetSessionKey: resets the sessionKey in database if logged in
	function resetSessionKey() {
		if ($this->status == AM_STATUS_LOGGED_IN) {
			$this->sessionKey = $this->randSessionKey();
			$query = sprintf("UPDATE login SET sessionKey='%s' WHERE username='%s'", $this->sessionKey, $this->username);
			$this->database->exec($query);
			return true;
		}
		return false;
	}
	// updatePassword: updates a user's password if logged in
	function updatePassword($password) {
		if ($this->status == AM_STATUS_LOGGED_IN) {
			$this->setPassword($password);
			$query = sprintf("UPDATE login SET password='%s' WHERE username='%s'", $this->password, $this->username);
			$this->database->exec($query);
			return true;
		}
		return false;
	}
	// updateAuthPin: updates a user's authPin if logged in
	function updateAuthPin($authPin) {
		if ($this->status == AM_STATUS_LOGGED_IN) {
			$this->setAuthPin($authPin);
			$query = sprintf("UPDATE login SET authPin='%d' WHERE username='%s'", $this->authPin, $this->username);
			$this->database->exec($query);
			return true;
		}
		return false;
	}
	// updateStatus: updates a user's status if logged in
	function updateStatus($status) {
		if ($status < 10 || $status > 39) return false; // soft check for integrity
		if ($this->status == AM_STATUS_LOGGED_IN) {
			$query = sprintf("UPDATE login SET status='%d' WHERE username='%s'", $status, $this->username);
			$this->database->exec($query);
			return true;
		}
		return false;
	}
	// updateEmail: updates a user's email if logged in
	function updateEmail($email) {
		if ($this->status == AM_STATUS_LOGGED_IN) {
			$query = sprintf("UPDATE login SET email='%s' WHERE username='%s'", $email, $this->username);
			$this->database->exec($query);
			return true;
		}
		return false;
	}
	// updateOAuth: update a user's OAuth if logged in (experimental)
	function updateOAuth($key, $type) {
		if ($this->status == AM_STATUS_LOGGED_IN) {
			$query = sprintf("UPDATE login SET oauth='%s', oauth_type='%s' WHERE username='%s'", $key, $type, $this->username);
			$this->database->exec($query);
			return true;
		}
		return false;
	}
	// updateUserAccess: update a user's access level if logged in
	function updateUserAccess($access) {
		if ($access < 50 || $access > 99) return false; // soft check for integrity
		if ($this->status == AM_STATUS_LOGGED_IN) {
			$query = sprintf("UPDATE login SET user_access='%d' WHERE username='%s'", $access, $this->username);
			$this->database->exec($query);
			return true;
		}
		return false;
	}
	
	// hashify: returns a hashed string
	function hashify($string) {
		return sha1($string . UX_SALT);
	}
	// randSessionKey: generates a random sessionKey based on username, time and salt
	function randSessionKey() {
		return $this->username . ":" . sha1(microtime(false) . UX_SALT) . ":" . microtime(true);
	}
	
	// verifyLogin: verifies username+[sessionKey|password] and sets status accordingly
	function verifyLogin() {
		if ($this->sessionKey) {
			$query = sprintf("SELECT * FROM login WHERE username = '%s' AND sessionKey = '%s'", $this->username, $this->sessionKey);
			$result = $this->database->query($query);
			$row = $result->fetchArray();
			if (!$row) { // No match
				$this->status = AM_STATUS_NO_MATCH;
				return false;
			}
			else {
				$this->status = AM_STATUS_LOGGED_IN;
				$this->authMethod = "sessionKey";
				return true;
			}
		}
		elseif ($this->password) {
			$query = sprintf("SELECT * FROM login WHERE username = '%s' AND password = '%s'", $this->username, $this->password);
			$result = $this->database->query($query);
			$row = $result->fetchArray();
			if (!$row) { // No match
				$this->status = AM_STATUS_NO_MATCH;
				return false;
			}
			else {
				$this->status = AM_STATUS_LOGGED_IN;
				$this->authMethod = "password";
				return true;
			}
		}
		else {
			$this->status = AM_STATUS_NO_CREDENTIAL;
			return false;
		}
	}
	
	// userExists: check if username already exists
	function userExists($username) {
		$query = sprintf("SELECT * FROM login WHERE username = '%s'", $username);
		$result = $this->database->query($query);
		$row = $result->fetchArray();
		if (!$row) return false;
		return true;
	}
	// addUser: adds a new user to database
	function addUser($username, $password, $authPin, $email) {
		if ($this->userExists($username)) {
			return false;
		}
		$query = sprintf("INSERT INTO login (username, password, sessionKey, authPin, status, email, oauth, oauth_type, user_access) VALUES ('%s', '%s', '%s', '%d', '%d', '%s', '', '-1', '%d');", $username, $this->hashify($username . $password), $this->randSessionKey(), $authPin, AM_USER_UNVERIFIED, $email, AM_ACCESS_REGULAR);
		if ($this->database->exec($query)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	// initDB: initializes a new database (iff it does not exist)
	function initDB() {
		$query = "CREATE TABLE IF NOT EXISTS login (
				username STRING PRIMARY KEY,
				password STRING,
				sessionKey STRING,
				authPin INTEGER,
				status INTEGER,
				email STRING,
				oauth STRING,
				oauth_type STRING,
				user_access INTEGER
			);";
		$this->database->exec($query);
	}
}

// Public functions

// sanitize: sanitize a string for SQL
function sanitize($string) {
	return htmlspecialchars($string, ENT_QUOTES);
}
?>

<?php
$app = "Home";

if (isset($_GET["app"])) {
	if (file_exists(BIN_DIRECTORY . $_GET["app"] . ".php")) {
		$app = $_GET["app"];
	}
}

include(BIN_DIRECTORY . "HEADER.php");
include(BIN_DIRECTORY . $app . ".php");
include(BIN_DIRECTORY . "FOOTER.php");
?>

<!DOCTYPE html>
<html>
<head>
	<title>urusai.ninja</title>
	<style type="text/css">
		* { margin: 0; padding: 0; font-family: "Lato", sans-serif; }
		body { background-color: white; }
		a, a:visited { color: #078CFF; text-decoration: none; }
		a:hover { text-decoration: underline; }
		
		div#content { max-width: 800px; padding: 20px; margin: auto; }
		div.background { position: absolute; top: 0px; left: 0px; right: 0px; background-color: #212121; z-index: -1; }
		
		h1.header, h1.header a { font-size: 24px; color: #EFEFEF; }
		h1.header a:hover { color: white; text-decoration: none; }
		h1.header em { font-size: 36px; font-style: normal; font-weight: normal; }
		
		div#navigation { font-size: 14px; margin-bottom: 40px; }
		div#navigation a { text-decoration: none; color: #DDD; margin-right: 10px; }
		div#navigation a:hover { color: #078CFF; }
		
		div#footer { font-size: 14px; margin-top: 40px; }
		div#footer a { text-decoration: none; color: #222; margin-right: 10px; }
		div#footer a:hover { color: gray; }
		
		div.debug { max-width: 800px; padding: 20px; margin: auto; margin-bottom: 10px; }
		div.debug ul { list-style: none; }
		div.debug.dbg_error { border: double 5px red; }
		div.debug.dbg_warn { border: double 5px yellow; }
		div.debug.dbg_info { border: double 5px cyan; }
		
		form.login { border: solid 1px #DEDEDE; padding: 20px; }
		form.login label { font-size: 14px; color: #666; }
		form.login input[type='text'], form.login input[type='password'] { display: block; margin: 2px 0px; width: 100%; box-sizing: border-box; font-size: 24px; padding: 8px; }
		form.login .button { display: block; width: 250px; height: 32px; padding: 10px 25px; background-color: #212121; border: none; text-align: center; margin: auto; color: white; }
		form.login .button:hover { color: #078CFF; }
		
		p.error { background-color: #DD5656; padding: 6px; }
	</style>
	<script>
		
	</script>
	<link href='http://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
	<meta name="viewport" content="width=360, initial-scale=1, maximum-scale=1, minimum-scale=1">
	<link href='//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css' rel='stylesheet' type='text/css'>
</head>
<body>
	<div id="content">
		<div class="background" style="height: 100px;"></div>
		<h1 class="header"><a href='/'><em>urusai</em>.ninja</a></h1>
		<div id="navigation">
			<a href="/" class="current"><span class='fa fa-home'></span></a>
			<a href="/app.Anime">anime</a>
			<a href="/app.OST">ost</a>
<?php
	if (!$authID) {
		echo "
			<a href=\"//" . SRV_LOGIN . "/app.Login?redirect=" . SRV_HOST . "\">login</a>
		";
	}
	else {
		printf("
			<a href=\"//" . SRV_LOGIN . "/app.Profile\">%s</a>
			<a href=\"/core.LoginHandler?do=logout\" style='color: #DD5656;'><span class='fa fa-power-off'></span></a>
		", $_SESSION["authUser"]);
	}
?>
		</div>

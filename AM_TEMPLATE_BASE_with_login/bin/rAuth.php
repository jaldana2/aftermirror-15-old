<?php
if (isset($_GET["username"]) && isset($_GET["key"])) {
	session_start();
	$_SESSION["authUser"] = $_GET["username"];
	$_SESSION["authKey"] = $_GET["key"];
	$_SESSION["authCheck"] = sha1($_SESSION["authUser"] . $_SESSION["authKey"] . UX_SALT);
	header("Location: /app.Home?hello");
}
else {
	header("Location: /app.Home");
}
?>

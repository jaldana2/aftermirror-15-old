<?php
$app = "Home";
$launchMode = "app";

if (isset($_GET["core"])) {
	if (file_exists(BIN_DIRECTORY . $_GET["core"] . ".php")) {
		$app = $_GET["core"];
		$launchMode = "core";
	}
}
if (isset($_GET["app"])) {
	if (file_exists(BIN_DIRECTORY . $_GET["app"] . ".php")) {
		$app = $_GET["app"];
	}
}

if ($launchMode == "app") include(BIN_DIRECTORY . "HEADER.php");
include(BIN_DIRECTORY . $app . ".php");
if ($launchMode == "app") include(BIN_DIRECTORY . "FOOTER.php");
?>

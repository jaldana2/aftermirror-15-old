<?php
session_start();

define("ENGINE_PLUGIN_DIR", "../plugins/");
define("BIN_DIRECTORY", "bin/");

define("SRV_HOST", "entity.urusai.ninja");
define("SRV_LOGIN", "entity.aftermirror.com");

include("../engine.php");

$authID = false;

if (isset($_SESSION["authUser"]) && isset($_SESSION["authKey"]) && isset($_SESSION["authCheck"])) {
	$authID = file_get_contents("https://" . SRV_LOGIN . "/core.authID?do=VERIFY&username={$_SESSION['authUser']}&sessionKey={$_SESSION['authKey']}");
	if ($authID == "PASS") {
		session_write_close();
		define("UX_LOGGED_IN", true);
	}
	else {
		session_destroy();
		header("Location: /app.Home");
		die();
	}
}
if (!defined("UX_LOGGED_IN")) define("UX_LOGGED_IN", false);

include("loader.php");
?>
